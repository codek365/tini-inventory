
<div class="col-sm-12">
<button type="button" class="btn btn-success" id="add_variation" data-action="add"><i class="fa fa-plus"></i> @lang('product.add_variation')</button>
</div>

<div class="col-sm-12">
    <div class="table-responsive">
    <table class="table table-bordered add-product-price-table table-condensed" id="product_variation_form_part">
        <thead>
          <tr>
{{--            <th class="col-sm-1">Tên</th>--}}
            <th class="col-sm-2">Biến thể</th>
            <th class="col-sm-10"></th>
          </tr>
        </thead>
        <tbody>
            @if($action == 'add')
                @include('product.partials.product_variation_row', ['row_index' => 0])
            @else
                @forelse ($product_variations as $product_variation)
                    @include('product.partials.edit_product_variation_row', ['row_index' => $action == 'edit' ? $product_variation->id : $loop->index])
                @empty
                    @include('product.partials.product_variation_row', ['row_index' => 0])
                @endforelse

            @endif
            
        </tbody>
    </table>
    </div>
</div>