@inject('request', 'Illuminate\Http\Request')
<!-- Main Header -->
<header class="main-header no-print">
    <a href="/home" class="logo">
      	<span class="logo-lg">{{ Session::get('business.name') }}</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
			
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li>
					<a id="btnCalculator" type="button" class=" hidden-xs btn-sm popover-default" data-toggle="popover" data-trigger="click" data-content='@include("layouts.partials.calculator")' data-html="true" data-placement="bottom">
						<strong><i class="fa fa-calculator fa-lg" aria-hidden="true"></i></strong>
					</a>
				</li>

				@if($request->segment(1) == 'pos')
					<li>
						<a type="button" id="register_details" title="{{ __('cash_register.register_details') }}" data-toggle="tooltip" data-placement="bottom" class="pull-left hidden-xs btn-sm btn-modal" data-container=".register_details_modal" 
						data-href="{{ action('CashRegisterController@getRegisterDetails')}}">
							<strong><i class="fa fa-list-alt fa-lg" aria-hidden="true"></i></strong>
						</a>

						<a type="button" id="close_register" title="{{ __('cash_register.close_register') }}" data-toggle="tooltip" data-placement="bottom" class="pull-left hidden-xs btn-sm btn-modal" data-container=".close_register_modal" 
						data-href="{{ action('CashRegisterController@getCloseRegister')}}">
							<strong><i class="fa fa-lock fa-lg"></i></strong>
						</a>
					</li>	
				@endif

				@can('sell.create')
					<li>
						<a href="{{action('SellPosController@create')}}" title="POS" data-toggle="tooltip" data-placement="bottom" class="pull-left hidden-xs btn-sm">
							<strong><i class="fa fa-qrcode fa-lg"></i> &nbsp; </strong>
						</a>
					</li>
				@endcan
				@can('profit_loss_report.view')
					<li>
						<a type="button" id="view_todays_profit" title="{{ __('home.todays_profit') }}" data-toggle="tooltip" data-placement="bottom" class="pull-left hidden-xs btn-sm">
							<strong><i class="fa fa fa-bar-chart fa-lg"></i></strong>
						</a>
					</li>
				@endcan

				<!-- Help Button -->
				@if(auth()->user()->hasRole('Admin#' . auth()->user()->business_id))
					<li>
						<a type="button" id="start_tour" title="@lang('lang_v1.application_tour')" data-toggle="tooltip" data-placement="bottom" class="pull-left hidden-xs btn-sm">
							<strong><i class="fa fa-question-circle fa-lg" aria-hidden="true"></i></strong>
						</a>
					</li>
				@endif

				<!-- User Account Menu -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						@if(!empty(Session::get('business.logo')))
							<img src="{{ url( 'storage/business/' . Session::get('business.id') .'/img/' . Session::get('business.logo') ) }}" alt="{{Session::get('business.name')}}" alt="{{Session::get('business.name')}}" class="user-image" ></span>
						@endif
						<span>{{ Auth::User()->first_name }} {{ Auth::User()->last_name }}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- The user image in the menu -->
						<li class="user-header">
							@if(!empty(Session::get('business.logo')))
							<img src="{{ url( 'storage/business/' . Session::get('business.id') .'/img/' . Session::get('business.logo') ) }}" alt="{{Session::get('business.name')}}"></span>
							@endif
							<p>
							{{ Auth::User()->first_name }} {{ Auth::User()->last_name }}
							</p>
						</li>
						<!-- Menu Body -->
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
							<a href="{{action('UserController@getProfile')}}" class="btn btn-default btn-flat">@lang('lang_v1.profile')</a>
							</div>
							<div class="pull-right">
							<a href="{{action('Auth\LoginController@logout')}}" class="btn btn-default btn-flat">@lang('lang_v1.sign_out')</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
      	</div>
    </nav>
  </header>