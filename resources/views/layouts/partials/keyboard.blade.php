<script src="{{ asset('plugins/mousetrap/mousetrap.min.js?v=' . $asset_v) }}"></script>
<script type="text/javascript">
	$(document).ready( function() {
		//shortcut to go to sale page
		@if(!empty($shortcuts["pos"]["go_to_sale"]))
			Mousetrap.bind('{{$shortcuts["pos"]["go_to_sale"]}}', function(e) {
				window.location.href = $('#go-to-sale').attr('href');
			});
		@endif

		Mousetrap.bind('f7', function(e) {
			if($('#todays_profit_modal').hasClass('in')){
				$('#todays_profit_modal').modal('hide');
			}else{
				$('#view_todays_profit').trigger('click');
			}
		});
		
	});
</script>
