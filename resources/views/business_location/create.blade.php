<div class="modal-dialog" role="document">
	<div class="modal-content">

		{!! Form::open(['url' => action('BusinessLocationController@store'), 'method' => 'post', 'id' => 'business_location_add_form' ]) !!}

		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">@lang( 'business.add_business_location' )</h4>
		</div>

		<div class="modal-body">
		<div class="row">
			<div class="col-sm-12">
			<div class="form-group">
				{!! Form::label('name', __( 'business.business_location_name' ) . ':*') !!}
				{!! Form::text('name', null, ['class' => 'form-control', 'required', 'placeholder' => __( 'invoice.name' ) ]); !!}
			</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('location_id', __( 'lang_v1.location_id' ) . ':') !!}
				{!! Form::text('location_id', null, ['class' => 'form-control', 'placeholder' => __( 'lang_v1.location_id' ) ]); !!}
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('landmark', __( 'business.landmark' )  . ':*') !!}
				{!! Form::text('landmark', null, ['class' => 'form-control', 'placeholder' => __( 'business.landmark' ), 'required' ]); !!}
			</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('city', __( 'business.city' ) . ':*') !!}
				{!! Form::select('city', $cities, 79, ['class' => 'form-control select2_register','placeholder' => __('business.city'), 'required']); !!}
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('mobile', __( 'business.mobile' ) . ':*') !!}
				{!! Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => __( 'business.mobile'), 'required' ]); !!}
			</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('email', __( 'business.email' ) . ':') !!}
				{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => __( 'business.email')]); !!}
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('website', __( 'lang_v1.website' ) . ':') !!}
				{!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => __( 'lang_v1.website')]); !!}
			</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('invoice_scheme_id', __('invoice.invoice_scheme') . ':*') !!} @show_tooltip(__('tooltip.invoice_scheme'))
				{!! Form::select('invoice_scheme_id', $invoice_schemes, 1, ['class' => 'form-control', 'required',
				'placeholder' => __('messages.please_select')]); !!}
			</div>
			</div>
			<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('invoice_layout_id', __('invoice.invoice_layout') . ':*') !!} @show_tooltip(__('tooltip.invoice_layout'))
				{!! Form::select('invoice_layout_id', $invoice_layouts, 1, ['class' => 'form-control', 'required',
				'placeholder' => __('messages.please_select')]); !!}
			</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12">
			<hr/>
		</div>
		</div>
		</div>

		<div class="modal-footer">
		<button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
		</div>

		{!! Form::close() !!}

	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->