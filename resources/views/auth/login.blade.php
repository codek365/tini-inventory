@inject('request', 'Illuminate\Http\Request')
@extends('layouts.auth')
@php
    $pos_layout = false;
@endphp
@section('title', __('lang_v1.login'))

@section('content')

<div class="container">
    
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
            @if(config('app.env') == 'demo')
                <div class="panel-heading"><h3>Cửa hàng mẫu <small><i> Giúp bạn có cái nhìn tổng quan về các chức năng của ứng dụng </i></small></h3></div>
                <div class="panel-body">
                    <div class="text-center" id="glyphs">
                    {{-- @include('layouts.partials.header-login') --}}
                        <a href="?demo_type=pharmacy" data-toggle="tooltip" title="Dùng cho cửa hàng bán các sản phẩm có hạn sử dụng"." >
                            <i class="flaticon-drug-1"></i>
                            <span>Nhà thuốc</span>
                        </a>
                        <a href="?demo_type=services" data-toggle="tooltip" title="Dùng cho các nhà cung cấp dịch vụ như làm đẹp, sửa chữa điện tử ...">
                            <i class="flaticon-robot"></i>
                            <span>Dịch vụ</span>
                        </a>
                        <a href="?demo_type=electronics" data-toggle="tooltip" title="Dùng cho cửa hàng bán các sản phẩm có IMEI hoặc Serial code." >
                            <i class="flaticon-kitchen"></i>
                            <span>Điện thoại & Điện máy</span>
                        </a>
                        <a href="?demo_type=super_market" data-toggle="tooltip" title="Dùng cho cửa hàng tạp hóa hoặc siêu thị"." >
                            <i class="flaticon-shop-1"></i>
                            <span>Siêu thị mini</span>
                        </a>
                        <a href="?demo_type=restaurant" data-toggle="tooltip" title="Dùng cho quán ăn nhà hàng"." >
                            <i class="flaticon-break-1"></i>
                            <span>Cafe & nhà hàng</span>
                        </a>
                    </div>
                </div>
            @else
                <div class="panel-heading"><h3>@lang('lang_v1.login')</h3></div>
            @endif
                <div class="panel-body">
                    
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">@lang('lang_v1.username')</label>

                            <div class="col-md-6">
                                @php
                                    $username = old('username');
                                    $password = null;
                                    if(config('app.env') == 'demo'){
                                        $username = 'admin';
                                        $password = 'admin123';

                                        $demo_types = array(
                                            'all_in_one' => 'admin',
                                            'super_market' => 'admin',
                                            'pharmacy' => 'pharmacy',
                                            'electronics' => 'electronics',
                                            'services' => 'services',
                                            'restaurant' => 'restaurant',
                                            'superadmin' => 'superadmin'
                                        );
                                        if( !empty($_GET['demo_type']) && array_key_exists($_GET['demo_type'], $demo_types) ){
                                            $username = $demo_types[$_GET['demo_type']];
                                        }
                                    }
                                @endphp
                                <input id="username" type="text" class="form-control" name="username" value="{{ $username }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('lang_v1.password')</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password"
                                value="{{ $password }}" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('lang_v1.remember_me')
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                @if(config('app.env') != 'demo')
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        @lang('lang_v1.forgot_your_password')
                                    </a>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    @lang('lang_v1.login')
                                </button>
                                @if(!($request->segment(1) == 'business' && $request->segment(2) == 'register'))

                                    <!-- Register Url -->
                                    @if(env('ALLOW_REGISTRATION', true))
                                        <a 
                                            href="{{ route('business.getRegister') }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif"
                                            class="btn bg-maroon btn-flat margin" 
                                        ><b>{{ __('business.register_now') }}</b></a>

                                        <!-- pricing url -->
                                        @if(Route::has('pricing') && config('app.env') != 'demo' && $request->segment(1) != 'pricing')
                                            <a href="{{ action('\Modules\Superadmin\Http\Controllers\PricingController@index') }}">@lang('superadmin::lang.pricing')</a>
                                        @endif
                                    @endif
                                @endif

                                @if(!($request->segment(1) == 'business' && $request->segment(2) == 'register') && $request->segment(1) != 'login')
                                    {{ __('business.already_registered')}} <a href="{{ action('Auth\LoginController@login') }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif">{{ __('business.sign_in') }}</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
         </div>
    </div>           
    
   
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('#change_lang').change( function(){
            window.location = "{{ route('login') }}?lang=" + $(this).val();
        });
    })
</script>
@endsection
