<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Purchase Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Purchase screens
    |
    */

    'supplier' => 'Nhà cung cấp',
    'ref_no' => 'Mã hợp đồng',
    'business_location' => 'Địa điểm',
    'purchases' => 'Đơn hàng',
    'add_payment' => 'Thêm thanh toán',
    'edit_payment' => 'Sửa thanh toán',
    'view_payments' => 'Xem các thanh toán',
    'amount_already_paid' => 'Tiền đã thanh toán',
    'payment_added_success' => 'Thêm thanh toán thành công',
    'payment_updated_success' => 'Cập nhật thanh toán thành công',
    'payment_deleted_success' => 'Xóa thanh toán thành công',
    'view_payments' => 'Xem các thanh toán',
    'amount' => 'Amount',
    'payment_method' => 'Phương thức thanh toán',
    'no_records_found' => 'Không tìm thấy kết quả',
    'payment_due' => 'Nợ cần thanh toán',
    'purchase_total' => 'Tổng nhập',
    'location' => 'Chi nhánh',
    'payment_note' => 'Ghi chú thanh toán',
    'all_purchases' => 'Tất cả nhập',
    'purchase_status' => 'Trạng thái nhập',
    'payment_status' => 'Trạng thái',
    'grand_total' => 'Tổng cộng',
    'add_purchase' => 'Tạo đơn hàng',
    'purchase_date' => 'Ngày nhập',
    'purchase_quantity' => 'Số lượng nhập',
    'unit_cost_before_tax' => 'Giá mỗi sản phẩm (trước thuế)',
    'subtotal_before_tax' => 'Thành tiền (trước thuế)',
    'product_tax' => 'Thuế sản phẩm',
    'net_cost' => 'Giá cố định',
    'line_total' => 'Thành tiền',
    'unit_selling_price' => 'Giá bán',
    'total_before_tax' => 'Tổng trước thuế',
    'net_total_amount' => 'Tổng thu hộ',
    'discount_type' => 'Kiểu giảm giá',
    'discount_amount' => 'Tiền giảm',
    'discount' => 'Giảm giá',
    'edit_purchase' => 'Sửa nhập hàng',
    'purchase_tax' => 'Thuế nhập',
    'shipping_details' => 'Thông tin vận chuyển',
    'additional_shipping_charges' => 'Thêm chi phí vận chuyển',
    'purchase_total' => 'Tổng nhập hàng',
    'additional_notes' => 'Ghi chú',
    'purchase_add_success' => 'Tạo nhập hàng thành công',
    'purchase_update_success' => 'Sửa nhập hàng thành công',
    'purchase_details' => 'Thông tin nhập hàng',
    'unit_cost_after_tax' => 'Giá sản phẩm (Sau thuế)',
    'total_after_tax' => 'Tổng cộng sau thuế',
    'no_payments' => 'Không tìm thấy thanh toán',
    'allow_purchase_different_currency' => 'Purchases in other currency',
    'purchase_currency' => 'Tiền tệ nhập',
    'p_exchange_rate' => 'Tỉ xuất ',
    'diff_purchase_currency_help' => 'Tiền tệ nhập hiện tại là <strong> :currency </strong>',
    'list_purchase' => 'Danh sách đơn hàng',
    'attach_document' => 'Chứng từ đính kèm',
    'download_document' => 'Tải chứng từ',
    'max_file_size' => 'Dung lượng tải tối đa: :sizeMB',
];
