<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tax Rates Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Tax Rate CRUD operations.
    |
    */

    'tax_rates' => 'Thuế xuất',
    'manage_your_tax_rates' => 'Quản lý thuế',
    'all_your_tax_rates' => 'Danh sách thuế',
    'name' => 'Tên thuế',
    'rate' => 'Giá trị %',
    'added_success' => 'Thêm thuế xuất thành công',
    'updated_success' => 'Cập nhật thuế xuất thành công',
    'deleted_success' => 'Xóa thuế xuất thành công',
    'add_tax_rate' => 'Thêm thuế thành công',
    'edit_taxt_rate' => 'Sửa thuế thành công',
    'add_tax_group' => 'Thêm nhóm thuế',
    'tax_group_added_success' => 'Thêm nhóm thuế thành công',
    'tax_group_updated_success' => 'Sửa nhóm thuế thành công',
    'tax_groups' => 'Nhóm thuế xuất',
    'edit_tax_group' => 'Sửa nhóm thuế',
    'sub_taxes' => 'Sub taxes',
    'can_not_be_deleted' => 'Thuế xuất này đang được thêm trong nhóm thuế'

];
