<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during common CRUD operations.
    |
    */

    'add' => 'Thêm mới',
    'edit' => 'Sửa',
    'save' => 'Lưu',
    'update' => 'Cập nhật',
    'action' => 'Thao tác',
    'actions' => 'Thao tác',
    'view' => 'Xem',
    'delete' => 'Xóa',
    'close' => 'Đóng',
    'something_went_wrong' => 'Có lỗi vui lòng quay lại sau',
    'required' => 'Trường này là bắt buộc',
    'please_select' => 'Vui lòng chọn',
    'cancel' => 'Hủy',
    'date' => 'Ngày',
    'filter_by_date' => 'Filter by date',
    'location' => 'Chi nhánh',
    'go_back' => 'Trở lại',
    'due_tooltip' => 'Dấu (-) = Tiền trả <br> Dấu (+) = Tiền nhận',
    'purchase_due_tooltip' => 'Dấu (-) = Tiền nhận <br> Dấu (+) = Tiền trả',
    'yes' => 'Yes',
    'no' => 'No',
    'all' => 'Tất cả',
    'settings' => 'Cài đặt',
    'business_location_settings' => 'Cài đặt chi nhánh',
    'print' => 'Print',
    'submit' => 'Xác nhận',
    'purchase_sell_mismatch_exception' => 'Lỗi: Không khớp giữa số lượng bán và số lượng mua. Sản phẩm: :product',
    'purchase_stock_adjustment_mismatch_exception' => 'Lỗi: Không khớp giữa số điều chỉnh và số lượng mua. Sản phẩm: :product',
    'transaction_edit_not_allowed' => 'Giao dịch không được sửa sau :days ngày.',
    'name' => 'Tên',
    'price' => 'Giá',
    'activate' => 'Kích hoạt',
    'deactivate' => 'Chưa kích hoạt',
];
