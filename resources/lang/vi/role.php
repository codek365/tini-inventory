<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Brand CRUD operations.
    |
    */

    'add_role' => 'Thêm vai trò',
    'edit_role' => 'Sửa vai trò',
    'user' => 'Người dùng',
    'supplier' => 'Nhà cung cấp',
    'customer' => 'Khách hàng',
    'purchase' => 'Mua hàng',
    'report' =>'Báo cáo',

    'user.view' => 'Xem người dùng',
    'user.create' => 'Thêm người dùng',
    'user.update' => 'Sửa người dùng',
    'user.delete' => 'Xóa người dùng',

    'supplier.view' => 'Xem thông tin nhà cung cấp',
    'supplier.create' => 'Thêm nhà cung cấp',
    'supplier.update' => 'Sửa nhà cung cấp',
    'supplier.delete' => 'Xóa nhà cung cấp',

    'customer.view' => 'Xem thông tin khách hàng',
    'customer.create' => 'Thêm khách hàng',
    'customer.update' => 'Sửa khách hàng',
    'customer.delete' => 'Xóa khách hàng',

    'product.view' => 'Xem sản phẩm',
    'product.create' => 'Thêm sản phẩm',
    'product.update' => 'Sửa sản phẩm',
    'product.delete' => 'Xóa sản phẩm',

    'purchase.view' => 'Xem mua hàng và kiểm kho',
    'purchase.create' => 'Thêm mua hàng và kiểm kho',
    'purchase.update' => 'Sửa mua hàng và kiểm kho',
    'purchase.delete' => 'Xóa mua hàng và kiểm kho',

    'sell.view' => 'Xem bán hàng POS',
    'sell.create' => 'Thêm bán hàng POS',
    'sell.update' => 'Sửa bán hàng POS',
    'sell.delete' => 'Xóa bán hàng POS',

    'purchase_n_sell_report.view' => 'Xem báo cáo mua bán',
    'contacts_report.view' => 'Xem báo cáo khách hàng và nhà cung cấp',
    'stock_report.view' => 'Xem báo cáo kho hàng, kiểm hàng và hàng hóa hết hạn',
    'tax_report.view' => 'Xem báo cáo thuế',
    'trending_product_report.view' => 'Xem báo cáo sản phẩm bán chạy',
    'register_report.view' => 'Xem báo cáo đăng ký',
    'sales_representative.view' => 'Xem báo cáo người bán hàng',
    'expense_report.view' => 'Xem báo cáo tiền chi',

    'business_settings.access' => 'Cài đặt doanh nghiệp',
    'barcode_settings.access' => 'Cài đặt mã vạch',
    'invoice_settings.access' => 'Cài đặt hóa đơn',

    'brand.view' => 'Xem nhãn hàng',
    'brand.create' => 'Thêm nhãn hàng',
    'brand.update' => 'Sửa nhãn hàng',
    'brand.delete' => 'Xóa nhãn hàng',

    'tax_rate.view' => 'Xem thuế xuất',
    'tax_rate.create' => 'Thêm thuế xuất',
    'tax_rate.update' => 'Sửa thuế xuất',
    'tax_rate.delete' => 'Xóa thuế xuất',

    'unit.view' => 'Xem đơn vị tính',
    'unit.create' => 'Thêm đơn vị tính',
    'unit.update' => 'Sửa đơn vị tính',
    'unit.delete' => 'Xóa đơn vị tính',

    'category.view' => 'Xem danh mục',
    'category.create' => 'Thêm danh mục',
    'category.update' => 'Sửa danh mục',
    'category.delete' => 'Xóa danh mục',
    'select_all' => 'Chọn tất cả',
    'settings' => 'Cài đặt',
    'brand' => 'Nhãn hàng',
    'tax_rate' => 'Thuế xuất',
    'unit' => 'Đơn vị tính',
    'expense.access' => 'Tiền chi',
    'access_locations' =>'Vị trí',
    'all_locations' => 'Tất cả các vị trí',
    'dashboard' => 'Trang chủ',
    'dashboard.data' => 'Xem thống kê trên trang chủ',
    'profit_loss_report.view' => 'Xem báo cáo lợi nhuận',
    'direct_sell.access' => 'Bán hàng',
];
