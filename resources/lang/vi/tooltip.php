<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tooltip Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various help texts.
    |
    */

    'product_stock_alert' => "Sản phẩm còn ít.<br/><small class='text-muted'>Dựa trên thông báo số lượng thiết lập khi thêm sản phẩm.<br> Nhập thêm sản phẩm này trước khi hết hàng trong kho.</small>",

    'payment_dues' => "Các thanh toán nhập hàng cần phải trả. <br/><small class='text-muted'>Dự vào hạn thanh toán của các nhà cung cấp. <br/> Hiển thị các thanh toán phải trả trong 7 ngày hoặc thấp hơn.</small>",

    'input_tax' => 'Total tax collected for sales within the selected time period.',

    'output_tax' => 'Total tax paid towards purchases for the selected time period.',

    'tax_overall' => 'Difference between total tax collected and total tax paid within the selected time period.',

    'purchase_due' => 'Tổng tiền nhập hàng chưa thanh toán.',

    'sell_due' => 'Tổng tiền bán hàng',

    'over_all_sell_purchase' => '-ve value = Amount to pay <br>+ve Value = Amount to receive',

    'no_of_products_for_trending_products' => 'Số sản phẩm hiển thị trong đồ thị sản phẩm bán chạy.',

    'top_trending_products' => "Sản phẩm bán chạy. <br/><small class='text-muted'>Cho phép bạn lọc sản phẩm bán chạy theo danh mục, thương hiệu, chi nhánh ...</small>",

    'sku' => "Mã số duy nhất cho mỗi sản phẩm <br><br>Để rỗng để tạo SKU tự động.<br><small class='text-muted'>Bạn có thể sửa tiền tố SKU trong cài đặt.</small>",

    'enable_stock' => "Cho phép hoặc vô hiệu hóa quản lý sản phẩm này trong kho hàng. <br><br><small class='text-muted'>Quản lý kho hàng thì không cần thiết trong một số dịch vụ . Ví dụ: Sửa chữa, Cắt tóc ...</small>",

    'alert_quantity' => "Nhận thông báo khi số lượng sản phẩm hiện tại còn lại dưới số lượng này.<br><br><small class='text-muted'>Các sản phẩm này sẽ xuất hiện trong trang chủ - Sản phẩm sắp hết.</small>",

    'product_type' => '<b>Sản phẩm thường</b>: Sản phẩm không có thuộc tính.
    <br><b>Sản phẩm biến thể</b>: Sản phẩm có các thuộc tính như màu sắc, kích thước ...',

    'profit_percent' => "Lợi nhuận mặc định cho sản phẩm. <br><small class='text-muted'>(<i>Bạn có thể cài đặt lợi nhuận mặc định trong trang Cài đặt cửa hàng.</i>)</small>",

    'pay_term' => "Khoảng thời gian phải thanh toán sau khi nhập hàng.<br/><small class='text-muted'>Các thanh toán đến hạn này sẽ hiển thị trên trang chủ - Công nợ</small>",

    'order_status' => 'Các sản phẩm nhập chỉ có thể bán khi <b>Trạng thái nhập</b> là <b>Đã nhận</b>.',

    'purchase_location' => 'Chi nhánh nơi sản phẩm nhập được chuyển đến để bán.',

    'sale_location' => 'Chi nhánh bạn muốn bán hàng',

    'sale_discount' => "Đặt 'Giảm giá mặc định' cho tất cả các sản phẩm trong cài đặt doanh nghiệp. Nhấn vào biểu tượng sửa bên dưới để thêm hoặc sửa giảm giá.",

    'sale_tax' => "Cài đặt 'Thuế bán mặc định' cho tất cả các sản phẩm trong cài đặt doanh nghiệp. Nhấn vào biểu tượng sửa bên dưới để thêm hoặc sửa thuế bán.",

    'default_profit_percent' => "Lợi nhuận mặc định. <br><small class='text-muted'>Dùng để tính giá bán cơ bản dựa trên giá mua.<br/> Giá trị này có thể sửa khi thêm sản phẩm</small>",

    'fy_start_month' => 'Chọn tháng đầu tiên của năm để làm kế toán cho doanh nghiệp',

    'business_tax' => 'Mã số thuế của doanh nghiệp.',

    'invoice_scheme' => "Định dạng số hóa đơn. Chọn định dạng hóa đơn để sử dụng cho doanh nghiệp<br><small class='text-muted'><i>Bạn có thể thêm định dạng hóa đơn </b> trong cài đặt hóa đơn</i></small>",

    'invoice_layout' => "Mẫu hóa đơn <br><small class='text-muted'>(<i><b>Mẫu hóa đơn</b> có thể thêm mới trong <b>Cài đặt hóa đơn<b></i>)</small>",

    'invoice_scheme_name' => 'Give a short meaningful name to the Invoice Scheme.',

    'invoice_scheme_prefix' => 'Tiền tố cho định dạng số hóa đơn.<br>Tiền tố có thể là  chuỗi ký tự hoặc năm hiện tại. Ví dụ: #XXXX0001, #2018-0002',

    'invoice_scheme_start_number' => "Số đầu tiên của hóa đơn. <br><small class='text-muted'>Bạn có thể đặt là 1 hoặc bất kỳ số nào bạn muốn.</small>",

    'invoice_scheme_count' => 'Tổng số hóa đơn được tạo cho định dạng này',

    'invoice_scheme_total_digits' => 'Độ dài ký tự của số hóa đơn bao gồm tiền tố hóa đơn',

    'tax_groups' => 'Group Tax Rates - defined above, to be used in combination in Purchase/Sell sections.',

    'unit_allow_decimal' => "Số thập phân cho phép.",

    'print_label' => 'Thêm các sản phẩm ->Chọn các giá trị hiển thị -> Chọn cài đặt mã vạch -> Xem trước -> In',

    'expense_for' => 'Chọn nhân viên liên quan đến tiền chi. <i>(Optional)</i><br/><small>Ví dụ: Lương nhân viên.</small>',
    
    'all_location_permission' => 'Nếu <b>Tất cả các chi nhánh</b> được chọn thì vai trò này sẽ áp dụng cho tất cả',

    'dashboard_permission' => 'Nếu bỏ chọn chỉ lời chào được hiển thị trên trang chủ.',

    'access_locations_permission' => 'Choose all locations this role can access. All data for the selected location will only be displayed to the user.<br/><br/><small>For Example: You can use this to define <i>Store Manager / Cashier / Stock manager / Branch Manager, </i>of particular Location.</small>',

    'print_receipt_on_invoice' => 'Enable or Disable auto-printing of invoice on finalizing',

    'receipt_printer_type' => "<i>Browser Based Printing</i>: Show print dialogue box in browser with preview of invoice <br/><br/> <i>Use Configured Receipt Printer</i>: Select a configured receipt/thermal printer for printing",

    'adjustment_type' => '<i>Bình thường</i>: Điều chỉnh vì mất mát, hư hỏng , hết hạn sử dụng .... <br/><br/> <i>Bất thường</i>: Điều chỉnh do hỏa hoạn, thiên tai gây hư hỏng sản phẩm.',

    'total_amount_recovered' => 'Tiền được nhận lại từ bảo hiểm, bán sắt vụn ... ',

    'express_checkout' => 'Đánh dấu thanh toán hoàn tất',
    'total_card_slips' => 'Tổng số giao dịch bằng chuyển khoản được thực hiện tại quầy',
    'total_cheques' => 'Tổng số chi phiếu thực hiện thạn toán tại quầy',

    'capability_profile' => "Support for commands and code pages varies between printer vendors and models. If you're not sure, it's a good idea to use the 'simple' Capability Profile",

    'purchase_different_currency' => 'Select this option if you purchase in a different currency than your business currency',

    'currency_exchange_factor' => "1 Purchase Currency = ? Base Currency <br> <small class='text-muted'>You can enable/disabled 'Purchase in other currency' from business settings.</small>",

    'accounting_method' => 'Phương thức kế toán',

    'transaction_edit_days' => 'Số ngày sau khi giao dịch được thực hiện vẫn có thể chỉnh sửa.',
    'stock_expiry_alert' => "Danh sách các sản phẩm trong kho hết hạn trong :days ngày <br> <small class='text-muted'>Bạn có thể cài đặt ngày hết hạn trong cài đặt doanh nghiệp </small>",
    'sub_sku' => "Sku là 1 tùy chọn. <br><br><small>Để trống để tạo SKU tự động.<small>",
    'shipping' => "Thêm thông tin và phí vận chuyển. Nhấn vào biểu tượng chỉnh sửa để thêm hoặc sửa thông tin và phí vận chuyển."
];
