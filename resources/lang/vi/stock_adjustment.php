<?php

return [

    /*
    |--------------------------------------------------------------------------
    | kiểm kho Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for kiểm kho CRUD operations.
    |
    */

    'stock_adjustment' => 'Kiểm kho',
    'stock_adjustments' => 'Kiểm kho',
    'list' => 'Kiểm kho',
    'add' => 'Thêm kiểm kho',
    'all_stock_adjustments' => 'Tất cả kiểm kho',
    'search_product' => 'Tìm kiếm sản phẩm cần kiểm kho',
    'adjustment_type' => 'Kiểu kiểm kho',
    'normal' => 'Bình thường',
    'abnormal' => 'Bất thường',
    'total_amount' => 'Tổng tiền',
    'total_amount_recovered' => 'Tiền thu hồi',
    'reason_for_stock_adjustment' => 'Lý do chỉnh kho',
    'stock_adjustment_added_successfully' => 'Thêm kiểm kho thành công',
    'search_products' => 'Tìm kiếm sản phẩm',
    'delete_success' => 'Xóa kiểm kho thành công',
    'view_details' => 'Xem kiểm kho'
];