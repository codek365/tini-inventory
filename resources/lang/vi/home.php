<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in home page.
    |
    */
    'home' => 'Trang chủ',
    'welcome_message' => 'Xin chào :name,',
    'total_sell' => 'Tổng doanh số',
    'total_purchase' => 'Các thanh toán',
    'invoice_due' => 'Hóa đơn đến hạn',
    'purchase_due' => 'Thánh toán đến hạn',
    'today' => 'Hôm nay',
    'this_week' => 'Trong tuần',
    'this_month' => 'Trong tháng',
    'this_fy' => 'Trong năm',
    'sells_last_30_days' => 'Bán hàng trong 30 ngày qua',
    'sells_current_fy' => 'Bán hàng trong năm',
    'total_sells' => 'Tổng cộng (:currency)',
    'product_stock_alert' => 'Sản phẩm sắp hết',
    'payment_dues' => 'Công nợ',
    'due_amount' => 'Tiền nợ',
    'stock_expiry_alert' => 'Thông báo hết hạn',
    'todays_profit' => "Lợi nhuận trong ngày",
    ];