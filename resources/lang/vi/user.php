<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Business Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration of a business.
    |
    */
    'edit_profile' => 'Sửa thông tin',
    'change_password' => 'Thay đổi mật khẩu',
    'current_password' => 'Mật khẩu hiện tại',
    'new_password' => 'Mật khẩu mới',
    'confirm_new_password' => 'Xác nhận mật khẩu mới',
    'user_management' => 'Thành viên',
    'roles' => 'Các vai trò',
    'users' => 'Danh sách thành viên',
    'manage_roles' => 'Quản lý vai trò',
    'all_roles' => 'Tất cả vai trò',
    'add_role' => 'Thêm vai trò',
    'role_name' => 'Tên vai trò',
    'permissions' => 'Phân quyền',
    'role_added' => 'Thêm vai trò thành công',
    'role_already_exists' => 'Tên vai trò đã tồn tại',
    'edit_role' => 'Sửa vai trò',
    'role_updated' => 'Cập nhật vai trò thành công',
    'role_deleted' => 'Xóa vai trò thành công',
    'manage_users' => 'Quản lý thành viên',
    'all_users' => 'Tất cả thành viên',
    'name' =>'Tên người dùng',
    'role' => 'Vai trò người dùng',
    'add_user' => 'Thêm thành viên',
    'user_added' => 'Thêm thành viên thành công',
    'role_is_default' => 'Bạn không thể sửa vai trò này',
    'edit_user' => 'Sửa thành viên',
    'leave_password_blank' => "Để trống mật khẩu nếu bạn không muốn thay đổi",
    'user_update_success' => 'Cập nhật thành viên thành công',
    'user_delete_success' => 'Xóa thành viên thành công',
];
