<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Brand Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Brand CRUD operations.
    |
    */
    
    'barcodes' => 'Quản lý mã vạch',
    'barcode_settings' => 'Cài đặt mã vạch',
    'manage_your_barcodes' => 'Quản lý mã vạch',
    'all_your_barcode' => 'Các cài đặt',
    'setting_name' => 'Sticker Sheet setting Name',
    'setting_description' => 'Sticker Sheet setting Description',
    'added_success' => 'Barcode setting added successfully',
    'updated_success' => 'Barcode setting updated successfully',
    'deleted_success' => 'Barcode setting deleted successfully',
    'add_new_setting' => 'Add new setting',
    'add_barcode_setting' => 'Add barcode sticker setting',
    'edit_barcode_setting' => 'Edit barcode sticker setting',
    'in_in' => 'In Inches',
    'width' => 'Width of sticker',
    'height' => 'Height of sticker',
    'top_margin' => 'Additional top margin',
    'left_margin' => 'Additional left margin',
    'row_distance' => 'Distance between two rows',
    'col_distance' => 'Distance between two columns',
    'blank_stickers' => 'Blank Stickers',
    'stickers_in_one_row' => 'Stickers in one row',
    'delete_confirm' => 'This setting will be deleted \n Are you sure?',
    'set_as_default' => 'Đặt làm mặc định',
    'default' => 'Mặc định',
    'default_set_success' => 'Cài làm mặc định thành công',
    'stickers_in_one_sheet' => 'No. of Stickers per sheet',
    'is_continuous' => 'Continous feed or rolls',
    'products' => 'Sản phẩm',
    'no_of_labels' => 'Nhập số nhãn cần in',
    'info_in_labels' => 'Thông tin hiển thị trong nhãn sản phẩm',
    'print_name' => 'Tên sản phẩm',
    'print_variations' => 'Biến thể sản phẩm (khuyên dùng)',
    'print_price' => 'Giá sản phẩm',
    'show_price' => 'Hiện giá',
    'preview' => 'Xem trước',
    'print_business_name' => 'Tên doanh nghiệp',
    'barcode_setting' => 'Cài đặt mã vạch',
    'paper_width' => 'Chiều rộng',
    'paper_height' => 'Chiều cao',
    'print_labels' => 'Mã  vạch',
    'labels' => 'Nhãn',
];
