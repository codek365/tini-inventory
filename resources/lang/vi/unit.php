<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Unit Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Units CRUD operations.
    |
    */

    'units' => 'Đơn vị tính',
    'manage_your_units' => 'Quản lý đơn vị tính',
    'all_your_units' => 'Danh sách đơn vị tính',
    'name' => 'Tên đơn vị',
    'short_name' => 'Ký hiệu',
    'allow_decimal' => 'Cho phép thập phân',
    'added_success' => 'Thêm đơn vị tính thành công',
    'updated_success' => 'Cập nhật đơn vị thành công',
    'deleted_success' => 'Xóa đơn vị thành công',
    'add_unit' => 'Thêm đơn vị tính',
    'edit_unit' => 'Sửa đơn vị tính',

];
