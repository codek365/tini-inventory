<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Brand Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Brand CRUD operations.
    |
    */

    'sale' => 'Bán hàng',
    'sells' => 'Danh sách đơn hàng',
    'list_sale' => 'Danh sách đơn hàng',
    'add_sale' => 'Tạo đơn hàng',
    'pos_sale' => 'Bán hàng POS',
    'draft_added' => 'Lưu tạm thành công',
    'invoice_added' => 'Tạo hóa đơn thành công',
    'item' => 'SL',
    'total' => 'Tổng tiền',
    'order_tax' => 'Thuế',
    'discount' => 'Giảm giá',
    'total_payable' => 'Khách hàng phải trả',
    'cancel' => 'Hủy',
    'draft' => 'Lưu tạm',
    'finalize' => 'Thanh toán',
    'express_finalize' => 'Tiền mặt',
    'product' => 'Sản phẩm',
    'products' => 'Sản phẩm',
    'unit_price' => 'Đơn giá',
    'qty' => 'Số lượng',
    'subtotal' => 'Tổng cộng',
    'recent_transactions' => 'Các giao dịch gần đây',
    'pos_sale_added' => 'Hoàn thành bán hàng',
    'price_inc_tax' => 'Giá bán',
    'tax' => 'Thuế',
    'edit_discount' => 'Sửa giảm giá',
    'edit_order_tax' => 'Sửa thuế',
    'discount_type' => 'Loại giảm giá',
    'discount_amount' => 'Số tiền giảm',
    'no_recent_transactions' => 'Không có giao dịch gần đây',
    'final' => 'Hoàn thành',
    'invoice_no' => 'Số hóa đơn',
    'customer_name' => 'Tên khách hàng',
    'payment_status' => 'Trạng thái',
    'status' => 'Trạng thái',
    'total_amount' => 'Tổng tiền',
    'total_paid' => 'Tổng tiền đã thanh toán',
    'total_remaining' => 'Tổng tiền còn thiếu',
    'payment_info' => 'Thông tin thanh toán',
    'drafts' => 'Lưu tạm',
    'all_drafts' => 'Lưu tạm',
    'sell_details' => 'Dữ liệu bán hàng',
    'payments' => 'Các thanh toán',
    'amount' => 'Số tiền',
    'payment_mode' => 'Chế độ thanh toán',
    'payment_note' => 'Ghi chú thanh toán',
    'sell_note' => 'Ghi chú bán hàng',
    'staff_note' => 'Ghi chú nháp',
    'draft_updated' => 'Cập nhật nháp thành công',
    'pos_sale_updated' => 'Cập nhật bán hàng thành công',
    'location' => 'Vị trí',
    'add_payment_row' => 'Thêm thanh toán',
    'finalize_payment' => 'Quyết toán',
    'sale_date' => 'Ngày bán',
    'list_pos' => 'Đơn hàng POS',
    'edit_sale' => 'Sửa bán hàng',
    'shipping' => 'Vận chuyển',
    'shipping_details' => 'Chi tiết vận chuyển',
    'shipping_charges' => 'Chi phí vận chuyển'
];
