<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Receipt Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in receipt
    |
    */

    'product' => 'Sản phẩm',
    'qty' => 'Số lượng',
    'unit_price' => 'Đơn giá',
    'subtotal' => 'Tổng cộng',
    'discount' => 'Giảm giá',
    'tax' => 'Thuế',
    'total' => 'Tổng',
    'invoice_number' => 'Mã hóa đơn',
    'date' => 'Ngày',
    'receipt_settings' => 'Cài dặt hóa đơn',
    'receipt_settings_mgs' => 'Các cài đặt hóa đơn liên quan đến chi nhánh này',
    'print_receipt_on_invoice' => 'Tự động in hóa đơn sau khi tính tiền',
    'receipt_printer_type' => 'Kiểu in hóa đơn',
    'receipt_settings_updated' => 'Sửa cài đặt hóa đơn thành công'
];
