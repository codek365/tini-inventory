<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Product related operations.
    |
    */

    'exc_of_tax' => 'Không thuế',
    'inc_of_tax' => 'Có thuế',
	'profit_percent' => 'Lợi nhuận(%)',
	'profit' => 'Lợi nhuận',
    'add_new_product' => 'Thêm sản phẩm',
    'add_product_for_labels' => 'Chọn các sản phẩm cần dán nhãn',
    'product_type' => 'Kiểu sản phẩm',
    'category' => 'Danh mục sản phẩm',
    'sub_category' => 'Danh mục con',
    'unit' => 'Đơn vị tính',
    'brand' => 'Nhãn hàng',
    'tax' => 'Thuế',
    'sku' => 'SKU',
    'alert_quantity' => 'Thông báo số lượng',
    'product_name' => 'Sản phẩm',
    'auto_generate' => 'Tự động tạo',
    'manage_stock' => 'Theo dõi tồn kho?',
    'enable_stock_help' => 'Cho phép quản lý  số lượng hàng hóa trong kho',
    'barcode_type' => 'Kiểu Barcode',
    'applicable_tax' => 'Loại thuế',
    'selling_price_tax_type' => 'Cách tính giá bán',
    'inclusive' => 'Bao gồm thuế',
    'exclusive' => 'Không bao gồm thuế',
    'edit_product' => "Sửa sản phẩm",
    'default_purchase_price' => 'Giá sản phẩm',
	'default_selling_price' => 'Giá bán',
	'help_selling_price' => 'Giá bán = Giá sản phẩm (có thuế hoặc không thuế ) + % lợi nhuận mong muốn',
    'value' => 'Biến thể',
    'variation_name' => 'Tên thuộc tính',
    'variation_values' => 'Giá trị',
    'use_template' => 'Sử dụng thuộc tính mẫu',
    'add_variation' => 'Thêm biến thể',
    'product_added_success' => 'Thêm sản phẩm thành công',
    'product_updated_success' => 'Sửa sản phẩm thành công',
    'enable_product_expiry' => 'Sản phẩm có hạn sử dụng',
    'expiry_period' => 'Ngày hết hạn',
    'expires_in' => 'Expires in',
    'not_applicable' => 'Không cho phép',
    'months' => 'Tháng',
    'days' => 'Ngày',
    'mfg_date' => 'MFG Date',
    'exp_date' => 'Ngày hết hạn',
    'view_product' => 'Xem',
    'add_product' => 'Thêm sản phẩm',
    'variations' => 'Thuộc tính sản phẩm',
    'import_products' => 'Import sản phẩm',
    'file_to_import' => 'File để nhập sản phẩm',
    'file_imported_successfully' => 'Nhập file thành công',
    'download_csv_file_template' => 'Download file CSV mẫu',
 ];
