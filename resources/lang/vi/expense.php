<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Expense Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Expense CRUD operations.
    |
    */

    'expenses' => 'Tiền chi',
    'expense_categories' => 'Danh mục',
    'manage_your_expense_categories' => 'Quản lý danh mục chi',
    'all_your_expense_categories' => 'Tất cả danh mục',
    'expense_category' => 'Danh mục',
    'category_name' => 'Tên',
    'category_code' => 'Mã',
    'added_success' => 'Thêm danh mục chi thành công',
    'updated_success' => 'Cập nhật danh mục thành công',
    'deleted_success' => 'Xóa danh mục thành công',
    'add_expense_category' => 'Thêm danh mục tiền chi',
    'edit_expense_category' => 'Sửa danh mục',
    'all_expenses' => 'Các khoản chi',
    'expense_status' => 'Trạng thái',
    'add_expense' => 'Thêm tiền chi',
    'edit_expense' => 'Sửa tiền chi',
    'expense_note' => 'Ghi chú chi',
    'expense_add_success' => 'Tiền chi thêm thành công',
    'expense_update_success' => 'Cập nhật tiền chi thành công',
    'expense_delete_success' => 'Xóa tiền chi thành công',
    'expense_for' => 'Người nhận',

];
