<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contact Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Contacts CRUD operations.
    |
    */

    'contacts' => 'Đối tác, khách hàng ',
    'name' => 'Tên khách hàng',
    'contact' => 'Đối tác',
    'manage_your_contact' => 'Quản lý :contacts',
    'all_your_contact' => 'Tất cả :contacts',
    'add_contact' => 'Thêm khách hàng',
    'contact_type' => 'Kiểu khách hàng',
    'tax_no' => 'Mã số thuế',
    'pay_term' => 'Hạn thanh toán',
    'pay_term_period' => 'Kỳ hạn thanh toán',
    'mobile' => 'Di động',
    'landline' => 'Điện thoại bàn',
    'alternate_contact_number' => 'Điện thoại dự phòng',
    'edit_contact' => 'Sửa đối tác',
    'added_success' => 'Thêm đối tác thành công',
    'updated_success' => 'Cập nhật đối tác thành công',
    'deleted_success' => 'Xóa đối tác thành công',
    'add_new_customer' => 'Thêm khách hàng',
    'view_contact' => 'Xem đối tác',
    'contact_info' => 'Chi tiết :contact ',
    'all_purchases_linked_to_this_contact' => 'Các giao dịch mua với đối tác này',
    'all_sells_linked_to_this_contact' => 'Các giao dịch bán với đối tác này',
    'total_purchase_due' => 'Tổng nợ nhập hàng',
    'pay_due_amount' => 'Số tiền phải trả',
    'total_paid' => 'Tổng tiền thu',
    'total_purchase_paid' => 'Tổng thanh toán mua hàng',
    'total_sale_paid' => 'Tổng thánh toán bán hàng',
    'total_sale_due' => 'Tổng nợ bán hàng',
    'customer' => 'Khách hàng',
];
