<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Quầy tính tiền Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Quầy tính tiền CRUD operations.
    |
    */

    'cash_register' => 'Quầy tính tiền',
    'manage_your_cash_register' => 'Quản lý quầy tính tiền',
    'all_your_cash_register' => 'Quầy tính tiền',
    'cash_in_hand' => 'Tiền trong quầy',
    'open_cash_register' => 'Mở quầy tính tiền',
    'enter_amount' => 'Nhập số tiền',
    'open_register' => 'Bán hàng',
    'register_details' => 'Thông tin bán hàng',
    'cash_payment' => 'Tiền mặt',
    'checque_payment' => 'Chi phiếu',
    'card_payment' => 'Thẻ visa',
    'bank_transfer' => 'Chuyển khoản',
    'other_payments' => 'Phương thức khác',
    'total_sales' => 'Tổng đơn hàng',
    'total_cash' => 'Tổng tiền mặt',
    'current_register' => 'Hiện tại',
    'close_register' => 'Đóng quầy',
    'total_card_slips' => 'Số hóa đơn',
    'total_cheques' => 'Tổng chi phiếu',
    'closing_note' =>'Ghi chú',
    'close_success' => 'Đóng quầy thành công',
    'open' => 'Mở quầy',
    'close' => 'Đóng quầy',
    'total_refund' => 'Tiền trả lại',
    'refunds' => 'Hoàn tiền',

];
