<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Brand Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Brand CRUD operations.
    |
    */

    'brands' => 'Nhãn hàng',
    'manage_your_brands' => 'Quản lý nhãn hàng',
    'all_your_brands' => 'Các nhãn hàng',
    'note' => 'Ghi chú',
    'brand_name' => 'Tên nhãn hàng',
    'short_description' => 'Mô tả',
    'added_success' => 'Thêm nhãn hàng thành công',
    'updated_success' => 'Cập nhật nhãn hàng thành công',
    'deleted_success' => 'Xóa nhãn hàng thành công',
    'add_brand' => 'Thêm nhãn hàng',
    'edit_brand' => 'Sửa nhãn hàng',

];
