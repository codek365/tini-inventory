<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Invoice Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for invoice screen
    |
    */
    'invoice_settings' => 'Cài đặt hóa đơn',
    'manage_your_invoices' => 'Quản lý cài đặt hóa đơn',
    'all_your_invoice_schemes' => 'Tất cả các hóa đơn',
    'added_success' => 'Thêm hóa đơn thành công',
    'updated_success' => 'Cập nhật thành công',
    'deleted_success' => 'Xóa thành công',
    'add_invoice' => 'Thêm định giạng số hóa đơn',
    'edit_invoice' => 'Sửa định giạng số hóa đơn',
    'name' => 'Tên ',
    'prefix' => 'Tiền tố',
    'start_number' => 'Bắt đầu từ',
    'total_digits' => 'Số ký tự',
    'preview' => 'Xem trước',
    'not_selected' => 'Bỏ chọn',
    'invoice_count' => 'Invoice Count',
    'invoice_schemes' => 'Định dạng số hóa đơn',
    'invoice_layouts' => 'Mẫu hóa đơn',
    'invoice_layout' => 'Mãu hóa đơn',
    'all_your_invoice_layouts' => 'Tất cả các mẫu',
    'add_invoice_layout' => 'Thêm mẫu mới',
    'layout_name' => 'Tên mẫu hóa đơn',
    'invoice_scheme' => 'Định dạng số hóa đơn',
    'header_text' => 'Header text',
    'invoice_no_prefix' => 'Invoice no. label',
    'invoice_heading' => 'Invoice heading',
    'sub_total_label' =>'Subtotal label',
    'discount_label' => 'Discount label',
    'tax_label' => 'Tax label',
    'total_label' => 'Total label',
    'fields_to_be_shown_in_address' => 'Fields to be shown in location address',
    'highlight_color' => 'Highlight color',
    'footer_text' => 'Footer text',
    'layout_added_success' => 'Thêm mẫu hóa đơn thành công',
    'edit_invoice_layout' => 'Sửa mẫu hóa đơn thành công',
    'layout_updated_success' => 'Cập nhật mẫu hóa đơn thành công',
    'used_in_locations' => 'Chi nhánh đang sử dụng',
    'show_business_name' => 'Hiển thị tên doanh nghiệp',
    'show_location_name' => 'Hiển thị tên chi nhánh',
    'show_mobile_number' => 'Hiển thị số điện thoại',
    'show_alternate_number' => 'Hiển thị số điện thoại dự phòng',
    'show_email' => 'Thư điện tử',
    'show_tax_1' => 'Chi tiết thuế 1',
    'show_tax_2' => 'Chi tiết thuế 2',
    'fields_to_shown_for_communication' => 'Fields for Communication details',
    'fields_to_shown_for_tax' => 'Fields for Tax details',
    'invoice_logo' => 'Logo hóa đơn',
    'show_logo' => 'Hiển thị logo',
    'show_barcode' => 'Hiển thị mã vạch',
    'total_due_label' => 'Total Due Label',
    'invoice_heading_not_paid' => 'Heading Suffix for not paid',
    'invoice_heading_paid' => 'Heading Suffix for paid',
    'show_payments' => 'Hiển thị thông tin thanh toán',
    'show_customer' => 'Hiển thị thông tin khách hàng',
    'paid_label' => 'Amount Paid Label',
    'customer_label' => 'Customer Label'
];
