@extends('layouts.app')
@section('title', __('market::lang.sysnc'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('market::lang.sysnc') </h1>
</section>
<!-- Main content -->
<section class="content">
	<!-- Page level currency setting -->
	<input type="hidden" id="p_code" value="{{$currency_details->code}}">
	<input type="hidden" id="p_symbol" value="{{$currency_details->symbol}}">
	<input type="hidden" id="p_thousand" value="{{$currency_details->thousand_separator}}">
	<input type="hidden" id="p_decimal" value="{{$currency_details->decimal_separator}}">

	@include('layouts.partials.error')

	{!! Form::open(['url' =>  action('\Modules\Market\Http\Controllers\MarketController@update' , [$purchase->id] ), 'method' => 'PUT', 'id' => 'add_purchase_form', 'files' => true ]) !!}

	<input type="hidden" id="purchase_id" value="{{ $purchase->id }}">
	<div class="box box-solid">
		<div class="box-body">
		<div class="row">
			<div class="@if(!empty($default_purchase_status)) col-sm-4 @else col-sm-3 @endif">
			<div class="form-group">
				{!! Form::label('ref_no', __('market::lang.ref_no') . '*') !!}
				{!! Form::text('ref_no', $purchase->ref_no, ['class' => 'form-control', 'required']); !!}
			</div>
			</div>
			<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('location_id', __('market::lang.business_location').':*') !!}
				@show_tooltip(__('tooltip.purchase_location'))
				{!! Form::select('location_id', $business_locations, $purchase->location_id, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'disabled']); !!}
			</div>
			</div>
			<div class="@if(!empty($default_purchase_status)) col-sm-4 @else col-sm-3 @endif">
			<div class="form-group">
				{!! Form::label('transaction_date', __('market::lang.purchase_date') . ':*') !!}
				<div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</span>
				{!! Form::text('transaction_date', @format_date($purchase->transaction_date), ['class' => 'form-control', 'readonly', 'required']); !!}
				</div>
			</div>
			</div>
			
			<div class="col-sm-3 @if(!empty($default_purchase_status)) hide @endif">
			<div class="form-group">
				{!! Form::label('status', __('market::lang.purchase_status') . ':*') !!}
				@show_tooltip(__('tooltip.order_status'))
				{!! Form::select('status', $orderStatuses, $purchase->status, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select') , 'required']); !!}
			</div>
			</div>
		</div>
		</div>
	</div> <!--box end-->

	<div class="box box-solid">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-12">
					@include('market::partials.edit_purchase_entry_row')
					<hr/>
					<div class="pull-right col-md-5">
						<table class="pull-right col-md-12">
						<tr class="hide">
							<th class="col-md-7 text-right">@lang( 'market::lang.total_before_tax' ):</th>
							<td class="col-md-5 text-left">
							<span id="total_st_before_tax" class="display_currency"></span>
							<input type="hidden" id="st_before_tax_input" value=0>
							</td>
						</tr>
						<tr>
							<th class="col-md-7 text-right">@lang( 'market::lang.net_total_amount' ):</th>
							<td class="col-md-5 text-left">
							<span id="total_subtotal" class="display_currency">{{$purchase->total_before_tax/$purchase->exchange_rate}}</span>
							<!-- This is total before purchase tax-->
							<input type="hidden" id="total_subtotal_input" value="{{$purchase->total_before_tax/$purchase->exchange_rate}}" name="total_before_tax">
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<table class="table">
						<tr>
							<td colspan="4">
							<div class="form-group">
								{!! Form::label('additional_notes',__('market::lang.additional_notes')) !!}
								{!! Form::textarea('additional_notes', $purchase->additional_notes, ['class' => 'form-control', 'rows' => 3]); !!}
							</div>
							</td>
						</tr>
					</table>
					</div>
				</div>
			</div>
		</div><!--box end-->
	<div class="row">
		<div class="col-sm-12">
		<button type="button" id="submit_purchase_form" class="btn btn-primary pull-right btn-flat">@lang('messages.update')</button>
		</div>
	</div>
	{!! Form::close() !!}
</section>
<!-- /.content -->
<!-- quick product modal -->
<div class="modal fade quick_add_product_modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle"></div>
@endsection

@section('javascript')
  <script src="{{ asset('js/market.js?v=' . $asset_v) }}"></script>
  <script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
  <script type="text/javascript">
    $(document).ready( function(){
      update_table_total();
      update_grand_total();
    });
  </script>
@endsection
