@extends('layouts.app')

@section('title', __('market::lang.add_sale'))

@section('content')
<input type="hidden" id="__precision" value="{{config('constants.currency_precision')}}">
<!-- Main content -->
<section class="content no-print">
	<input type="hidden" id="item_addition_method" value="{{$business_details->item_addition_method}}">
	{!! Form::open(['url' => action('\Modules\Market\Http\Controllers\MarketController@addToCart'), 'method' => 'post', 'id' => 'add_sell_form' ]) !!}
	{!! Form::hidden('location_id', $default_location); !!}
	{!! Form::hidden('tax_rate_id', null); !!}
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">@lang('sale.add_sale')</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-sm-3">
						<div class="form-group">
							{!! Form::label('contact_id', __('contact.customer') . ':*') !!}
							<div class="input-group">
								<input type="hidden" id="default_customer_id" 
								value="{{ $walk_in_customer['id']}}" >
								<input type="hidden" id="default_customer_name" 
								value="{{ $walk_in_customer['name']}}" >
								{!! Form::select('contact_id', 
									[], null, ['class' => 'form-control mousetrap', 'id' => 'customer_id', 'placeholder' => 'Enter Customer name / phone', 'required']); !!}
								<span class="input-group-btn">
									<button type="button" class="btn btn-default bg-white btn-flat add_new_customer" data-name=""><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
								</span>
							</div>
						</div>

						<div class="form-group">
							{!! Form::label('transaction_date', __('sale.sale_date') . ':*') !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('transaction_date', @format_date('now'), ['class' => 'form-control', 'readonly', 'required']); !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('status', __('sale.status') . ':*') !!}
							{!! Form::select('status', ['ordered' => __('lang_v1.ordered'),'final' => __('sale.final'), 'draft' => __('sale.draft'), 'quotation' => __('lang_v1.quotation')], 'ordered', ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'required']); !!}
						</div>
					</div>

					<div class="col-sm-9">
						<div class="form-group">
							{!! Form::label('search_market_product', __('market::lang.search_products')) !!}
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-barcode"></i>
								</span>
								{!! Form::text('search_market_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_market_product', 'placeholder' => __('lang_v1.search_product_placeholder'),
								'disabled' => false,
								'autofocus' =>  true,
								]); !!}
							</div>
						</div>

						<div class="pos_product_div" style="min-height: 0">

							<input type="hidden" name="sell_price_tax" id="sell_price_tax" value="{{$business_details->sell_price_tax}}">

							<!-- Keeps count of product rows -->
							<input type="hidden" id="product_row_count" 
								value="0">
							@php
								$hide_tax = '';
								if( session()->get('business.enable_inline_tax') == 0){
									$hide_tax = 'hide';
								}
							@endphp
							<div class="table-responsive">
							<table class="table table-condensed table-bordered table-striped table-responsive" id="pos_table">
								<thead>
									<tr>
										<th class="text-center">	
											@lang('sale.product')
										</th>
										<th class="text-center">
											@lang('sale.qty')
										</th>
										<th class="text-center {{$hide_tax}}">
											@lang('sale.price_inc_tax')
										</th>
										<th class="text-center">
											@lang('sale.subtotal')
										</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
							</div>
							<div class="table-responsive">
							<table class="table table-condensed table-bordered table-striped">
								<tr>
									<td>
										<div class="pull-right"><b>@lang('sale.total'): </b>
											<span class="price_total">0</span>
										</div>
									</td>
								</tr>
							</table>
							</div>
						</div>
					
						<div class="box box-solid" id="payment_rows_div"><!--box start-->
							<div class="box-header">
								<h3 class="box-title">
									@lang('purchase.add_payment')
								</h3>
							</div>
							<div class="box-body payment_row">
								@include('sale_pos.partials.payment_row_form', ['row_index' => 0])
								<hr>
								<input type="hidden" name="final_total" id="final_total_input">
								<div class="row">
									<div class="col-sm-12">
										<div class="pull-right"><strong>@lang('lang_v1.balance'):</strong> <span class="balance_due">0</span></div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-12">
										<button type="button" id="submit-sell" class="btn btn-primary pull-right btn-flat">@lang('messages.submit')</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--box end-->
	
	{!! Form::close() !!}
</section>

<div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	@include('contact.create', ['quick_add' => true])
</div>
<!-- /.content -->
<div class="modal fade register_details_modal" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog" 
	aria-labelledby="gridSystemModalLabel">
</div>

@stop
@section('javascript')
	<script src="{{ asset('js/market.js?v=' . $asset_v) }}"></script>
	<script src="{{ asset('js/pos.js?v=' . $asset_v) }}"></script>
@endsection
