@extends('market::layouts.master')
@section('title', __('market::lang.add_product'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('market::lang.search_products')</h1>
</section>

<!-- Main content -->
<section class="content">

	<!-- Page level currency setting -->
	<input type="hidden" id="p_code" value="{{$currency_details->code}}">
	<input type="hidden" id="p_symbol" value="{{$currency_details->symbol}}">
	<input type="hidden" id="p_thousand" value="{{$currency_details->thousand_separator}}">
	<input type="hidden" id="p_decimal" value="{{$currency_details->decimal_separator}}">

	@include('layouts.partials.error')

	{!! Form::open(['url' => action('\Modules\Market\Http\Controllers\MarketController@search'), 'method' => 'post', 'id' => 'add_purchase_form', 'files' => true ]) !!}
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-search"></i></span>
								{!! Form::text('search_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_product', 'placeholder' => __('lang_v1.search_product_placeholder')]); !!}
							</div>
						</div>

						@if(!empty($business))
							{!! Form::select('supplier_id', $business, null, ['id' => 'supplier_id', 'class' => 'select2', 'name' => null, 'style' => 'width:25% !important']) !!}
						@endif

						@if (!empty($business_locations))
							{!! Form::select('location_id', $business_locations, null, ['id' => 'location_id', 'class' => 'select2', 'name' => null, 'style' => 'width:25% !important']) !!}
						@endif

						@if(!empty($categories))
							<select class="select2" id="product_category" style="width:25% !important">
								<option value="all">@lang('lang_v1.all_category')</option>
								@foreach($categories as $category)
									<option value="{{$category['id']}}">{{$category['name']}}</option>
								@endforeach
							</select>
						@endif

						@if(!empty($brands))
							{!! Form::select('size', $brands, null, ['id' => 'product_brand', 'class' => 'select2', 'name' => null, 'style' => 'width:23% !important']) !!}
						@endif

						{!! Form::hidden('transaction_date', @format_date('now')); !!}
						{!! Form::hidden('status', 'ordered'); !!}
					</div>
					<div class="box-body market-products" id="product_list_body"></div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</section>
</div>
<!-- /.content -->
<div class="modal fade quick_add_product_modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle"></div>
@endsection

@section('javascript')
	<script src="{{ asset('js/market.js?v=' . $asset_v) }}"></script>
	<script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
@endsection
