@extends('layouts.app')
@section('title', __('market::lang.add_product'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('market::lang.add_product')
</section>

<!-- Main content -->
<section class="content">
{!! Form::open(['url' => action('\Modules\Market\Http\Controllers\MarketController@store'), 'method' => 'post', 
'id' => 'product_add_form','class' => 'product_form', 'files' => true ]) !!}
	{!! Form::hidden('market_product_id', $duplicate_product->id); !!}
	{!! Form::hidden('supplier_sku', $duplicate_product->supplier_sku); !!}
	{!! Form::hidden('supplier_id', $duplicate_product->supplier_id); !!}
	{!! Form::hidden('type', $duplicate_product->type); !!}
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('name', __('product.product_name') . ':*') !!}
						{!! Form::text('name', $duplicate_product->market_name, ['class' => 'form-control', 'required',
						'placeholder' => __('product.product_name')]); !!}
					</div>
					<div class="form-group">
						{!! Form::label('unit_id', __('product.unit') . ':*') !!}
						<div class="input-group">
							{!! Form::select('unit_id', $units, session('business.default_unit'), ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'required']); !!}
							<span class="input-group-btn">
								<button type="button" @if(!auth()->user()->can('unit.create')) disabled @endif class="btn btn-default bg-white btn-flat btn-modal" data-href="{{action('UnitController@create', ['quick_add' => true])}}" title="@lang('unit.add_unit')" data-container=".view_modal"><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
							</span>
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('category_id', __('product.category') . ':') !!}
						{!! Form::select('category_id', $categories, null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
					</div>
					<div class="form-group">
						{!! Form::label('image', __('lang_v1.product_image') . ':') !!}
						{!! Form::file('image', ['id' => 'upload_image', 'accept' => 'image/*']); !!}
						<small><p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)]) <br> @lang('lang_v1.aspect_ratio_should_be_1_1')</p></small>
					</div>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">
								<table class="table table-bordered add-product-price-table table-condensed">
									
									@if ($duplicate_product->type == "single")
										<tr>
											<th>@lang('product.default_purchase_price')</th>
											<th>@lang('product.default_selling_price')</th>
										</tr>
										<tr>
											<td>
												{!! Form::text('purchase_price', @num_format($duplicate_product->purchase_price), ['class' => 'form-control input-sm dpp input_number', 'placeholder' => 'Excluding Tax', 'required']); !!}
											</td>
											<td>
												{!! Form::text('sell_price', @num_format($duplicate_product->sell_price), ['class' => 'form-control input-sm input_number', 'placeholder' => 'Including tax', 'id' => 'single_dsp_inc_tax', 'required']); !!}
											</td>
										</tr>
									@else
										<tr>
											<th>Biến thể</th>
											<th>Tên biến</th>
											<th>@lang('product.default_purchase_price')</th>
											<th>@lang('product.default_selling_price')</th>
										</tr>
										@php
											$variation_row_index = 0;
											$array_name = 'product_variation';
											$variation_array_name = 'variations';
											$row_index = 0;
											$sub_sku_required = '';
										@endphp
										<tr>
											<td class="hide">
												{!! Form::text($array_name . '[' . $row_index .'][name]', null, ['class' => 'form-control input-sm variation_name']); !!}
											</td>
											<td>
												{!! Form::select('template', $variation_templates, null, ['class' => 'form-control input-sm variation_template']); !!}
											</td>
											<td>
												{!! Form::text($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][value]', $variation->name, ['class' => 'form-control input-sm variation_value_name', 'required']); !!}
											</td>
											<td>
												{!! Form::hidden($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][sub_sku]', null); !!}
												{!! Form::text($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][default_purchase_price]', @num_format($duplicate_product->purchase_price), ['class' => 'form-control input-sm variable_dpp input_number', 'placeholder' => 'Excluding Tax', 'required']); !!}
												{!! Form::hidden($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][dpp_inc_tax]', @num_format($duplicate_product->purchase_price), ['class' => 'form-control input-sm variable_dpp_inc_tax input_number', 'placeholder' => 'Including Tax', 'required']); !!}
											</td>
											<td class="hide">
												{!! Form::text($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][profit_percent]', 10, ['class' => 'form-control input-sm variable_profit_percent input_number', 'required']); !!}
											</td>
											<td>
												{!! Form::text($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][default_sell_price]', @num_format($duplicate_product->sell_price), ['class' => 'form-control input-sm variable_dsp input_number', 'placeholder' => 'Excluding tax', 'required']); !!}
												{!! Form::hidden($array_name . '[' . $row_index .'][' . $variation_array_name . '][' . $variation_row_index . '][sell_price_inc_tax]', @num_format($duplicate_product->sell_price), ['class' => 'form-control input-sm variable_dsp_inc_tax input_number', 'placeholder' => 'Including tax', 'required']); !!}
											</td>
										</tr>
									@endif
								
								</table>
							</div>
						</div>
					</div>
					<input type="hidden" id="variation_counter" value="1">
					<input type="hidden" name="submit_type" id="submit_type">
					<button type="submit" value="submit" class="btn btn-primary submit_product_form">@lang('messages.save')</button>
				</div>
			</div>
		</div>
  	</div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection

@section('javascript')
  @php $asset_v = env('APP_VERSION'); @endphp
  <script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
@endsection