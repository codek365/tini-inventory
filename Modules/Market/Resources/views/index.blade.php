@extends('layouts.app')
@section('title', __('purchase.purchases'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>@lang('purchase.purchases')
        <small></small>
    </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
</section>

<!-- Main content -->
<section class="content no-print">
	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">@lang('market::lang.list_purchase')</h3>
            @can('purchase.create')
            	<div class="box-tools">
                    <a class="btn btn-block btn-success" href="{{action('\Modules\Market\Http\Controllers\MarketController@search')}}">
    				<i class="fa fa-plus"></i> @lang('messages.add')</a>
                </div>
            @endcan
        </div>
        <div class="box-body">
            @can('purchase.view')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                              <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                <i class="fa fa-caret-down"></i>
                              </button>
                            </div>
                          </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view table-text-center" id="product_table">
                        <thead>
                            <tr>
                                @can('product.delete')
                                    <th><input type="checkbox" id="select-all-row"></th>
                                @endcan
                                <th>&nbsp;</th>
                                <th>@lang('sale.product')</th>
                                <th>@lang('product.sku')</th>
                                <th>@lang('market::lang.supplier')</th>
                                <th>@lang('market::lang.purchase_price')</th>
                                <th>@lang('market::lang.selling_price')</th>
                                <th>@lang('messages.action')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            @endcan
        </div>
    </div>
</section>
<!-- /.content -->
@stop
@section('javascript')
<script src="{{ asset('js/market.js?v=' . $asset_v) }}"></script>
<script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
<script>
    $(document).ready( function(){
        var col_targets = [0, 6];
        var col_sort = [1, 'asc'];
        @can('product.delete')
            col_targets = [0, 1, 7];
            col_sort = [2, 'asc'];
        @endcan

        var product_table = $('#product_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/market',
            columnDefs: [ {
                "targets": col_targets,
                "orderable": false,
                "searchable": false
            } ],
            aaSorting: [col_sort],
            columns: [
                @can('product.delete')
                    { data: 'mass_delete'  },
                @endcan
                    { data: 'image', name: 'products.image'  },
                    { data: 'product', name: 'products.name'  },
                    { data: 'sku', name: 'products.sku'  },
                    { data: 'business_name', name: 'products.business_name'},
                    { data: 'purchase_price', name: 'products.purchase_price'},
                    { data: 'sell_price', name: 'products.sell_price'},
                    { data: 'action', name: 'action'}
                ],
                createdRow: function( row, data, dataIndex ) {
                    if($('input#is_rack_enabled').val() == 1){
                        var target_col = 0;
                        @can('product.delete')
                            target_col = 1;
                        @endcan
                        $( row ).find('td:eq('+target_col+') div').prepend('<i style="margin:auto;" class="fa fa-plus-circle text-success cursor-pointer no-print rack-details" title="' + LANG.details + '"></i>&nbsp;&nbsp;');
                    }
                    @can('product.delete')
                        $( row ).find('td:eq(0)').attr('class', 'selectable_td');
                    @endcan
                }
        });
    });
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        dateRangeSettings,
        function (start, end) {
            $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
            purchase_table.ajax.url( '/purchases?start_date=' + start.format('YYYY-MM-DD') +
                '&end_date=' + end.format('YYYY-MM-DD') ).load();
        }
    );
    $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
        purchase_table.ajax.url( '/purchases').load();
        $('#daterange-btn span').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
    });
</script>
	
@endsection