<div class="row">
	<div class="col-md-12 eq-height-row">
		@forelse($products as $product)
			<div class="col-md-3 col-xs-4 product_list no-print">
				<div class="product_box" data-variation_id="{{$product->variation_id}}" data-product_id="{{$product->product_id}}" title="{{$product->name}} @if($product->type == 'variable')- {{$product->variation}} @endif {{ '(' . $product->sub_sku . ')'}}">
				<div class="image-container">
					<img src="{{$product->image_url}}" alt="">
				</div>
					<h3 class="text text-muted text-uppercase">
						<small>{{$product->name}} 
						@if($product->type == 'variable')
							- {{$product->variation}}
						@endif
						</small>
						<small class="text-muted">
							({{$product->sub_sku}})
						</small>
					</h3>
					<button tabindex="-1" type="button" class="btn btn-link btn-modal" data-href="{{ action('\Modules\Market\Http\Controllers\MarketController@quickAdd', [$product->product_id, $product->variation_id]) }}" data-container=".quick_add_product_modal"><i class="fa fa-plus"></i> Thêm sản phẩm </button>
				</div>
			</div>
		@empty
			<h4 class="text-center">
				@lang('lang_v1.no_products_to_display')
			</h4>
		@endforelse
	</div>
	<div class="col-md-12">
		{{ $products->links('market::partials.product_list_paginator') }}
	</div>
</div>