<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    {!! Form::open(['url' => action('\Modules\Market\Http\Controllers\MarketController@saveQuickProduct'), 'method' => 'post', 'id' => 'quick_add_product_form' ]) !!}
	{!! Form::hidden('supplier_id', $product->business_id ); !!}
	{!! Form::hidden('variation_id', $product->variation_id ); !!}
	{!! Form::hidden('product_id', $product->product_id ); !!}
    <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      <h4 class="modal-title" id="modalTitle">@lang( 'product.add_new_product' )</h4>
    </div>
    <div class="modal-body">
      	<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('image', __('lang_v1.product_image') . ':') !!}
					@if(!empty($product->image_url))
						<div class="thumbnail">
							<img src="{{ url($product->image_url ) }}" class="product-thumbnail-smal">
						</div>
					@endif
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					@php 
						$p_name = $product->name;
						$p_sku = $product->sku;
						if($product->type == 'variable'){
							$p_name .= ' - '.$product->variation;
							$p_sku = $product->sub_sku;
						}
					@endphp
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('name', __('product.product_name') . ':*') !!}
							{!! Form::text('market_name', $p_name, ['class' => 'form-control', 'required',
							'placeholder' => __('product.product_name')]); !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('sku', __('product.sku')) !!} 
							{!! Form::text('supplier_sku', $p_sku, ['class' => 'form-control',
							'placeholder' => __('product.sku'), 'required', 'readonly']); !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered add-product-price-table table-condensed">
								<tr>
									<th>@lang('market::lang.purchase_price')</th>
									<th>@lang('market::lang.selling_price')</th>
								</tr>
								<tr>
									<td>
										{!! Form::text('purchase_price', $product->sell_price, ['class' => 'form-control input-sm  input_number', 'placeholder' => '','id' => 'purchase_price', 'required']); !!}
									<td>
										{!! Form::text('sell_price', null, ['class' => 'form-control input-sm input_number', 'placeholder' => '', 'id' => 'sell_price', 'required']); !!}
									</td>
								</tr>
							</table>
							</div>
					</div>
				</div>
			</div>
      	</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="submit_quick_product">@lang( 'messages.add' )</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

    {!! Form::close() !!}

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->