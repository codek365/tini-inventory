<?php

Route::group(['middleware' => 'web', 'prefix' => 'market', 'namespace' => 'Modules\Market\Http\Controllers'], function()
{
	Route::get('/search', 'MarketController@search');
	Route::get('/get_products', 'MarketController@getProducts');
	Route::get('/quick_add/{id}/{variation_id}', 'MarketController@quickAdd');
	Route::post('/save_quick_product', 'MarketController@saveQuickProduct');
	Route::post('/add_product/{id}', 'MarketController@addProduct');
	Route::get('/add_order', 'MarketController@addOrder');
	Route::get('/list', 'MarketController@getMarketProducts');
	Route::get('/get_item_cart/{variation_id}/{supplier_id}', 'MarketController@getProductRow');
	Route::post('/add_to_cart', 'MarketController@addToCart');
	Route::get('/orders', 'MarketController@getOrders');
	// Route::get('/get_suppliers', 'MarketController@getSuppliers');
	// Route::get('/sysnc/{id}', 'MarketController@sysnc');
	Route::resource('/', 'MarketController')->parameter('', 'market');
});