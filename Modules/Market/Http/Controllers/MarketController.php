<?php

	namespace Modules\Market\Http\Controllers;

	use App\Brands;
	use App\Category;
	use App\Contact;
	use App\Product;
	use App\User;
	use App\ProductVariation;
	use App\Variation;
	use App\TaxRate;
	use App\Transaction;
	use App\PurchaseLine;
	use App\BusinessLocation;
	use App\Business;
	use App\CustomerGroup;
	use App\Unit;
	use App\VariationTemplate;
	
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Routing\Controller;
	use Yajra\DataTables\Facades\DataTables;
	use Validator;
	
	use App\Utils\ContactUtil;
	use App\Utils\ProductUtil;
	use App\Utils\TransactionUtil;
	use App\Utils\BusinessUtil;
	use App\Utils\ModuleUtil;
	use Modules\Supplier\Entities\SupplierProducts;
	use Modules\Supplier\Entities\SupplierVariations;
	use Modules\Market\Entities\MarketProducts;
	
	class MarketController extends Controller
	{
		/**
		 * All Utils instance.
		 *
		 */
		protected $productUtil;
		protected $transactionUtil;
		protected $moduleUtil;
		protected $contactUtil;
    	protected $businessUtil;
		/**
		 * Constructor
		 *
		 * @param ProductUtils $product
		 * @return void
		 */
		public function __construct(ProductUtil $productUtil, TransactionUtil $transactionUtil, BusinessUtil $businessUtil, ModuleUtil $moduleUtil, ContactUtil $contactUtil)
		{
			$this->productUtil = $productUtil;
			$this->transactionUtil = $transactionUtil;
			$this->businessUtil = $businessUtil;
			$this->moduleUtil = $moduleUtil;
			$this->contactUtil = $contactUtil;
			
			$this->dummyPaymentLine = ['method' => 'cash', 'amount' => 0, 'note' => '', 'card_transaction_number' => '', 'card_number' => '', 'card_type' => '', 'card_holder_name' => '', 'card_month' => '', 'card_year' => '', 'card_security' => '', 'cheque_number' => '', 'bank_account_number' => '',
				'is_return' => 0, 'transaction_no' => ''];
		}
		
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index()
		{
			$business_id = request()->session()->get('user.business_id');
			if (request()->ajax()) {
				$products = MarketProducts::leftJoin('products', 'products.id', '=', 'market_products.product_id')
					->leftJoin('business', 'business.id', '=', 'market_products.supplier_id')
					->where('market_products.customer_id', $business_id)
					->where('products.type', '!=', 'modifier')
					->select(
						'products.id',
						'products.business_id',
						'market_products.status',
						'market_products.id as market_product_id',
						'market_products.market_name as product',
						'market_products.supplier_sku as sku',
						'business.name as business_name',
						'market_products.purchase_price as purchase_price',
						'market_products.sell_price as sell_price',
						'market_products.supplier_id as supplier_id',
						'products.image'
					);
				return Datatables::of($products)
					->addColumn(
						'action', function($row){
							if($row->status == 'selling')
								$html = '<span class="btn btn-xs btn-danger">' . __("market::lang.selling") . '</span>';
							else
								$html = '<a href="' . action('\Modules\Market\Http\Controllers\MarketController@create', ["market_product_id" => $row->market_product_id]) . '" class="btn btn-xs btn-success add-product"><i class="fa fa-plus"></i> ' . __("market::lang.add_to_warehouse") . '</a>';
							
							return $html;
						}
					)
					->editColumn('image', function ($row) {
						if(!empty($row->image)){
							$image_url = \Storage::url(sprintf(config('constants.business_img_path_format'),$row->business_id) . '/' . $row->image);
						} else {
							$image_url = asset('/img/default.png');
						}
						return '<div style="display: flex;"><img src="' . $image_url . '" alt="Product image" class="product-thumbnail-small"></div>';
					})
					// ->addColumn('purchase_price', function ($row) {
					// 	$purchase_price =  $row->purchase_price;
					// 	return '<span data-orig-value="' . $purchase_price . '" class="display_currency total-remaining" data-currency_symbol="true">' . $purchase_price . '</span>';
					// })
					->addColumn('mass_delete', function($row){
						if (auth()->user()->can("product.delete")) {
							return  '<input type="checkbox" class="row-select" value="' . $row->id .'">' ;
						} else {
							return '';
						}
					})
					
					->rawColumns(['action', 'image', 'mass_delete'])
					->make(true);
			}
			return view('market::index');
		}

		/**
		 * Checks if product sku already exists.
		 *
		 */
		public function hasProductSku($product_id, $sku)
		{
			$business_id = request()->session()->get('user.business_id');
			//check in products table
			$query = MarketProducts::where('product_id', $product_id)
							->where('supplier_sku', $sku);
			
			$count = $query->count();
			
			if ($count == 0) {
				return false;
			} else {
				return true;
			}
		}
		/**
		 * Retrieves products list.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function search()
		{
			if (!auth()->user()->can('market.create')) {
				abort(403, 'Unauthorized action.');
			}
			
			$business_id = request()->session()->get('user.business_id');
			
			//Check if subscribed or not
			if (!$this->moduleUtil->isSubscribed($business_id)) {
				return $this->moduleUtil->expiredResponse();
			}
			$currency_details = $this->transactionUtil->purchaseCurrencyDetails($business_id);
			$business_locations = BusinessLocation::forDropdown($business_id);
			
			$currency_details = $this->transactionUtil->purchaseCurrencyDetails($business_id);
			
			$default_purchase_status = null;
			if (request()->session()->get('business.enable_purchase_status') != 1) {
				$default_purchase_status = 'received';
			}
			$business = Business::where('id', '!=', $business_id)->pluck('name', 'id');
			$categories = Category::where('parent_id', 0);
			if(!empty($business_id)){
				$categories->where('business_id' , '!=', $business_id);
			}
			$categories = $categories
							->orderBy('name', 'asc')
							->get()
							->toArray();
			
			if(!empty($business_id)){
				$brands = Brands::where('business_id' , '!=', $business_id)->pluck('name', 'id');
			}else{
				$brands = Brands::pluck('name', 'id');
			}

			$brands->prepend(__('lang_v1.all_brands'), 'all');
			return view('market::search')
				->with(compact('business', 'currency_details', 'business_locations', 'categories', 'brands'));
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create()
		{
			if (!auth()->user()->can('product.create')) {
				abort(403, 'Unauthorized action.');
			}

			if(!empty(request()->input('market_product_id'))){
				$business_id = request()->session()->get('user.business_id');
				//Check if subscribed or not, then check for products quota
				if (!$this->moduleUtil->isSubscribed($business_id)) {
					return $this->moduleUtil->expiredResponse();
				} elseif (!$this->moduleUtil->isQuotaAvailable('products', $business_id)) {
					return $this->moduleUtil->quotaExpiredResponse('products', $business_id, action('ProductController@index'));
				}

				$categories = Category::where('business_id', $business_id)
									->where('parent_id', 0)
									->pluck('name', 'id');
				
				$units = Unit::where('business_id', $business_id)
									->pluck('short_name', 'id');
				//Get all business locations
				$duplicate_product = MarketProducts::where('id', request()->input('market_product_id'))->first();
				$variation = null;
				$variation_templates = null;
				if($duplicate_product->type == "variable"){
					$variation_templates = VariationTemplate::where('business_id', $business_id)->pluck('name', 'id')->toArray();
					$variation_templates = $variation_templates;
					$variation = Variation::where('id', $duplicate_product->variation_id)->first();
				}
				return view('market::create')->with(compact('categories', 'variation', 'units', 'duplicate_product', 'variation_templates'));
			}
		}
		
		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request)
		{
			if (!auth()->user()->can('product.create')) {
				abort(403, 'Unauthorized action.');
			}

			try {
				$business_id = $request->session()->get('user.business_id');
				$product_details = $request->only(['name', 'unit_id', 'category_id', 'type', 'barcode_type', 'sku']);
				$product_details['supplier_id'] = $request->input('supplier_id');
				$product_details['business_id'] = $business_id;
				$product_details['supplier_sku'] = $request->input('supplier_sku');
				$product_details['created_by'] = $request->session()->get('user.id');
				$product_details['enable_stock'] = 0 ;
				$market_product = MarketProducts::where('id', request()->input('market_product_id'))->first();
				if (empty($product_details['sku'])) {
					$product_details['sku'] = ' ';
				}

				$expiry_enabled = $request->session()->get('business.enable_product_expiry');
				
				//upload image
				if ($request->hasFile('image') && $request->file('image')->isValid()) {
					if ($request->image->getSize() <= config('constants.image_size_limit')) {
						$new_file_name = time() . '_' . $request->image->getClientOriginalName();
						$image_path = sprintf(config('constants.business_img_path_format'),$business_id);
						$path = $request->image->storeAs($image_path, $new_file_name);
						if ($path) {
							$product_details['image'] = $new_file_name;
						}
					}
				}

				DB::beginTransaction();

				$product = Product::create($product_details);

				if (empty(trim($request->input('sku')))) {
					$sku = $this->productUtil->generateProductSku($product->id);
					$product->sku = $sku;
					$product->save();
				}
				
				if ($product->type == 'single') {
					$this->productUtil->createSingleProductVariation($product->id, $product->sku, $request->input('purchase_price'), $request->input('purchase_price'), $request->input('profit_percent'), $request->input('sell_price'), $request->input('sell_price'));
				} elseif ($product->type == 'variable') {
					if (!empty($request->input('product_variation'))) {
						$input_variations = $request->input('product_variation');
						$this->productUtil->createVariableProductVariations($product->id, $input_variations);
					}
				}

				//Add product racks details.
				$product_racks = $request->get('product_racks', null);
				if (!empty($product_racks)) {
					$this->productUtil->addRackDetails($business_id, $product->id, $product_racks);
				}
				
				$market_product->status = 'selling';
				$market_product->save();
				DB::commit();
				$output = ['success' => 1,
								'msg' => __('product.product_added_success')
							];
			} catch (\Exception $e) {
				DB::rollBack();
				\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
				
				$output = ['success' => 0,
								'msg' => $e->getMessage()
							];
				return redirect('products')->with('status', $output);
			}

			if ($request->input('submit_type') == 'submit_n_add_opening_stock') {
				return redirect()->action(
					'OpeningStockController@add',
					['product_id' => $product->id]
				);
			} else if($request->input('submit_type') == 'submit_n_add_selling_prices'){
				return redirect()->action(
					'ProductController@addSellingPrices',
					[$product->id]
				); 
			} else if($request->input('submit_type') == 'save_n_add_another'){
				return redirect()->action(
					'ProductController@create'
				)->with('status', $output); 
			}

			return redirect('market')->with('status', $output);
		}
		
		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
		}
	
		public function getProducts(Request $request)
		{
			if ($request->ajax()) {
				$category_id = $request->get('category_id');
				$brand_id = $request->get('brand_id');
				$location_id = $request->get('location_id');
				$supplier_id = $request->get('supplier_id');

				$term = $request->get('term');
				
				$check_qty = false;
				$business_id = $request->session()->get('user.business_id');
				$products = Product::join(
					'variations',
					'products.id',
					'=',
					'variations.product_id'
				)
					->leftjoin(
						'variation_location_details AS VLD',
						function ($join) use ($location_id) {
							$join->on('variations.id', '=', 'VLD.variation_id');
							
							//Include Location
							if (!empty($location_id)) {
								$join->where(function ($query) use ($location_id) {
									$query->where('VLD.location_id', '=', $location_id);
									//Check null to show products even if no quantity is available in a location.
									//TODO: Maybe add a settings to show product not available at a location or not.
									$query->orWhereNull('VLD.location_id');
								});
								;
							}
						}
					)
					->where('products.type', '!=', 'modifier');

				if ($supplier_id != 0) {
					$products->where('products.business_id', $supplier_id);
				}else{
					$products->where('products.business_id', '!=', $business_id);
				}
				//Include search
				if (!empty($term)) {
					$products->where(function ($query) use ($term) {
						$query->where('products.name', 'like', '%' . $term .'%');
						$query->orWhere('sku', 'like', '%' . $term .'%');
						$query->orWhere('sub_sku', 'like', '%' . $term .'%');
					});
				}
				
				//Include check for quantity
				if ($check_qty) {
					$products->where('VLD.qty_available', '>', 0);
				}
				
				if ($category_id != 'all') {
					$products->where(function ($query) use ($category_id) {
						$query->where('products.category_id', $category_id);
						$query->orWhere('products.sub_category_id', $category_id);
					});
				}
				if ($brand_id != 'all') {
					$products->where('products.brand_id', $brand_id);
				}
				
				$products = $products->select(
					'products.id as product_id',
					'products.business_id',
					'products.name',
					'products.type',
					'products.enable_stock',
					'variations.id as variation_id',
					'variations.name as variation',
					'VLD.qty_available',
					'variations.default_sell_price as selling_price',
					'variations.sub_sku',
					'products.image'
				)
					->orderBy('products.name', 'asc')
					->groupBy('variations.id')
					->paginate(20);
				return view('market::partials.product_list')
					->with(compact('products'));
			}
		}

		/**
		 * Loads quick add product modal.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function quickAdd($id, $variation_id)
		{
			if (!auth()->user()->can('product.create')) {
				abort(403, 'Unauthorized action.');
			}
			$product = Product::join('variations', 'products.id', '=', 'variations.product_id')
			->where('products.id', $id)
			->where('variations.id', $variation_id);
			$product = $product->select(
					'products.id as product_id',
					'products.business_id',
					'products.name',
					'products.sku',
					'products.type',
					'products.enable_stock',
					'variations.id as variation_id',
					'variations.name as variation',
					'variations.default_sell_price as sell_price',
					'variations.sub_sku',
					'products.image'
				)->first();
			return view('market::partials.quick_add_product')
						->with(compact('product'));
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function saveQuickProduct(Request $request)
		{
			if (!auth()->user()->can('product.create')) {
				abort(403, 'Unauthorized action.');
			}
			
			try {
				$business_id = $request->session()->get('user.business_id');
				$product_details = $request->only(['market_name', 'supplier_id', 'supplier_sku', 'product_id', 'variation_id', 'purchase_price', 'sell_price']);
				if(!$this->hasProductSku($product_details['product_id'], $product_details['supplier_sku'])){
					$product_details['customer_id'] = $business_id;
					$product_details['created_by'] = $request->session()->get('user.id');
					DB::beginTransaction();

					$product = MarketProducts::create($product_details);

					if (empty(trim($request->input('supplier_sku')))) {
						$sku = $this->productUtil->generateProductSku($product->id);
						$product->supplier_sku = $sku;
						$product->save();
					}
					
					DB::commit();
					$output = ['success' => 1,
									'msg' => __('market::lang.product_added_success'),
									'product' => $product
								];
				}else{
					$output = ['success' => 0,
								'msg' => __("market::lang.exits_product")
							];
				}
			} catch (\Exception $e) {
				DB::rollBack();
				\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
				
				$output = ['success' => 0,
								'msg' => $e->getMessage()
							];
			}

			return $output;
		}

		/**
		 * Retrieves products list.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function getProduct()
		{
			if (request()->ajax()) {
				$term = request()->term;
				
				$check_enable_stock = true;
				if (isset(request()->check_enable_stock)) {
					$check_enable_stock = filter_var(request()->check_enable_stock, FILTER_VALIDATE_BOOLEAN);
				}
				
				if (empty($term)) {
					return json_encode([]);
				}
				
				$business_id = request()->session()->get('user.business_id');
				$q = SupplierProducts::leftJoin(
					'supplier_variations',
					'supplier_products.id',
					'=',
					'supplier_variations.product_id'
				)
					->where(function ($query) use ($term) {
						$query->where('supplier_products.name', 'like', '%' . $term . '%');
						$query->orWhere('sku', 'like', '%' . $term . '%');
						$query->orWhere('sub_sku', 'like', '%' . $term . '%');
					})
					->whereNull('supplier_variations.deleted_at')
					->select(
						'supplier_products.id as product_id',
						'supplier_products.name',
						'supplier_products.type',
						// 'products.sku as sku',
						'supplier_variations.id as variation_id',
						'supplier_variations.name as variation',
						'supplier_variations.sub_sku as sub_sku'
					)
					->groupBy('variation_id');
				
				if ($check_enable_stock) {
					$q->where('enable_stock', 1);
				}
				$products = $q->get();
				$products_array = [];
				foreach ($products as $product) {
					$products_array[$product->product_id]['name'] = $product->name;
					$products_array[$product->product_id]['sku'] = $product->sub_sku;
					$products_array[$product->product_id]['type'] = $product->type;
					$products_array[$product->product_id]['variations'][]
						= [
						'variation_id' => $product->variation_id,
						'variation_name' => $product->variation,
						'sub_sku' => $product->sub_sku
					];
				}
				
				$result = [];
				$i = 1;
				$no_of_records = $products->count();
				if (!empty($products_array)) {
					foreach ($products_array as $key => $value) {
						if ($no_of_records > 1 && $value['type'] != 'single') {
							$result[] = ['id' => $i,
								'text' => $value['name'] . ' - ' . $value['sku'],
								'variation_id' => 0,
								'product_id' => $key
							];
						}
						$name = $value['name'];
						foreach ($value['variations'] as $variation) {
							$text = $name;
							if ($value['type'] == 'variable') {
								$text = $text . ' (' . $variation['variation_name'] . ')';
							}
							$i++;
							$result[] = ['id' => $i,
								'text' => $text . ' - ' . $variation['sub_sku'],
								'product_id' => $key,
								'variation_id' => $variation['variation_id'],
							];
						}
						$i++;
					}
				}
				
				return json_encode($result);
			}
		}

		public function addOrder(){
			if (!auth()->user()->can('direct_sell.access')) {
				abort(403, 'Unauthorized action.');
			}

			$business_id = request()->session()->get('user.business_id');

			//Check if subscribed or not, then check for users quota
			if (!$this->moduleUtil->isSubscribed($business_id)) {
				return $this->moduleUtil->expiredResponse();
			} elseif (!$this->moduleUtil->isQuotaAvailable('invoices', $business_id)) {
				return $this->moduleUtil->quotaExpiredResponse('invoices', $business_id, action('SellController@index'));
			}

			$walk_in_customer = $this->contactUtil->getWalkInCustomer($business_id);
			
			$business_details = $this->businessUtil->getDetails($business_id);
			$taxes = TaxRate::forBusinessDropdown($business_id, true, true);

			$business_locations = BusinessLocation::forDropdown($business_id, false, true);
			$bl_attributes = $business_locations['attributes'];
			$business_locations = $business_locations['locations'];

			foreach ($business_locations as $id => $name) {
				$default_location = $id;
				break;
			}

			$commsn_agnt_setting = $business_details->sales_cmsn_agnt;
			$commission_agent = [];
			if ($commsn_agnt_setting == 'user') {
				$commission_agent = User::forDropdown($business_id);
			} elseif ($commsn_agnt_setting == 'cmsn_agnt') {
				$commission_agent = User::saleCommissionAgentsDropdown($business_id);
			}

			$types = [];
			if (auth()->user()->can('supplier.create')) {
				$types['supplier'] = __('report.supplier');
			}
			if (auth()->user()->can('customer.create')) {
				$types['customer'] = __('report.customer');
			}
			if (auth()->user()->can('supplier.create') && auth()->user()->can('customer.create')) {
				$types['both'] = __('lang_v1.both_supplier_customer');
			}
			$customer_groups = CustomerGroup::forDropdown($business_id);

			$payment_line = $this->dummyPaymentLine;
			$payment_types = $this->transactionUtil->payment_types();

			return view('market::create_order')
				->with(compact(
					'business_details',
					'taxes',
					'walk_in_customer',
					'business_locations',
					'bl_attributes',
					'default_location',
					'commission_agent',
					'types',
					'customer_groups',
					'payment_line',
					'payment_types'
				));
		}

		/**
		 * Store iterms .
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function addToCart(Request $request)
		{
			if (!auth()->user()->can('sell.create') && !auth()->user()->can('direct_sell.access')) {
				abort(403, 'Unauthorized action.');
			}

			$is_direct_sale = true;

			try {
				$input = $request->except('_token');
				if (!empty($input['products'])) {
					$business_id = $request->session()->get('user.business_id');

					$user_id = $request->session()->get('user.id');
					$commsn_agnt_setting = $request->session()->get('business.sales_cmsn_agnt');

					$invoice_total = $this->productUtil->calculateInvoiceTotal($input['products'], $input['tax_rate_id']);

					DB::beginTransaction();

					if (empty($request->input('transaction_date'))) {
						$input['transaction_date'] =  \Carbon::now();
					} else {
						$input['transaction_date'] = $this->productUtil->uf_date($request->input('transaction_date'));
					}
					
					$input['is_direct_sale'] = 1;

					$input['commission_agent'] = !empty($request->input('commission_agent')) ? $request->input('commission_agent') : null;
					
					if ($commsn_agnt_setting == 'logged_in_user') {
						$input['commission_agent'] = $user_id;
					}

					if(isset($input['exchange_rate']) && $this->transactionUtil->num_uf($input['exchange_rate']) == 0){
						$input['exchange_rate'] = 1;
					}
					
					//Customer group details
					$contact_id = $request->get('contact_id', null);
					$cg = $this->contactUtil->getCustomerGroup($business_id, $contact_id);
					$input['customer_group_id'] = (empty($cg) || empty($cg->id)) ? null : $cg->id;

					//set selling price group id
					if($request->has('price_group')){
						$input['selling_price_group_id'] = $request->input('price_group');
					}
					$input['sale_note'] = '';

					$transaction = $this->transactionUtil->createSellTransaction($business_id, $input, $invoice_total, $user_id);

					$this->transactionUtil->createOrUpdateSellLines($transaction, $input['products'], $input['location_id']);
					
					$this->transactionUtil->createOrUpdatePaymentLines($transaction, $input['payment']);

					DB::commit();
					
					$msg = trans("market::lang.order_added_success");
					$output = ['success' => 1, 'msg' => $msg, 'receipt' => '' ];
				} else {
					$output = ['success' => 0,'msg' => trans("messages.something_went_wrong")];
				}
			} catch (\Exception $e) {
				DB::rollBack();

				if (get_class($e) == \App\Exceptions\PurchaseSellMismatch::class) {
					$msg = $e->getMessage();
				} else {
					\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
					$msg = trans("messages.something_went_wrong");
				}

				$output = ['success' => 0,'msg' => trans("messages.something_went_wrong")];
			}

			return redirect('market/orders')->with('status', $output);
		}
		
		public function getOrders()
		{
			if (request()->ajax()) {
				$business_id = request()->session()->get('user.business_id');

				$sells = Transaction::leftJoin('contacts', 'transactions.contact_id', '=', 'contacts.id')
					->leftJoin('transaction_payments as tp', 'transactions.id', '=', 'tp.transaction_id')
					->join(
						'business_locations AS bl',
						'transactions.location_id',
						'=',
						'bl.id'
					)
					->where('transactions.business_id', $business_id)
					->where('transactions.type', 'sell')
					->where('transactions.status', 'ordered')
					->select(
						'transactions.id',
						'transaction_date',
						'is_direct_sale',
						'invoice_no',
						'contacts.name',
						'payment_status',
						'final_total',
						DB::raw('SUM(IF(tp.is_return = 1,-1*tp.amount,tp.amount)) as total_paid'),
						'bl.name as business_location'
					);

				$permitted_locations = auth()->user()->permitted_locations();
				if ($permitted_locations != 'all') {
					$sells->whereIn('transactions.location_id', $permitted_locations);
				}

				//Add condition for created_by,used in sales representative sales report
				if (request()->has('created_by')) {
					$created_by = request()->get('created_by');
					if (!empty($created_by)) {
						$sells->where('transactions.created_by', $created_by);
					}
				}

				//Add condition for location,used in sales representative expense report
				if (request()->has('location_id')) {
					$location_id = request()->get('location_id');
					if (!empty($location_id)) {
						$sells->where('transactions.location_id', $location_id);
					}
				}

				if (!empty(request()->customer_id)) {
					$customer_id = request()->customer_id;
					$sells->where('contacts.id', $customer_id);
				}
				if (!empty(request()->start_date) && !empty(request()->end_date)) {
					$start = request()->start_date;
					$end =  request()->end_date;
					$sells->whereDate('transaction_date', '>=', $start)
								->whereDate('transaction_date', '<=', $end);
				}

				//Check is_direct sell
				if (request()->has('is_direct_sale')) {
					$is_direct_sale = request()->is_direct_sale;
					if ($is_direct_sale == 0) {
						$sells->where('is_direct_sale', 0);
					}
				}

				//Add condition for commission_agent,used in sales representative sales with commission report
				if (request()->has('commission_agent')) {
					$commission_agent = request()->get('commission_agent');
					if (!empty($commission_agent)) {
						$sells->where('transactions.commission_agent', $commission_agent);
					}
				}
				$sells->groupBy('transactions.id');

				return Datatables::of($sells)
					->addColumn(
						'action',
						'<div class="btn-group">
						<button type="button" class="btn btn-info dropdown-toggle btn-xs" 
							data-toggle="dropdown" aria-expanded="false">' .
							__("messages.actions") .
							'<span class="caret"></span><span class="sr-only">Toggle Dropdown
							</span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right" role="menu">
						@if(auth()->user()->can("sell.view") || auth()->user()->can("direct_sell.access") )
							<li><a href="#" data-href="{{action(\'SellController@show\', [$id])}}" class="btn-modal" data-container=".view_modal"><i class="fa fa-external-link" aria-hidden="true"></i> @lang("messages.view")</a></li>
						@endif
						@if($is_direct_sale == 0)
							@can("sell.update")
							<li><a target="_blank" href="{{action(\'SellPosController@edit\', [$id])}}"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</a></li>
							@endcan
							@else
							@can("direct_sell.access")
								<li><a target="_blank" href="{{action(\'SellController@edit\', [$id])}}"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</a></li>
							@endcan
						@endif
						@can("sell.delete")
						<li><a href="{{action(\'SellPosController@destroy\', [$id])}}" class="delete-sale"><i class="fa fa-trash"></i> @lang("messages.delete")</a></li>
						@endcan

						@if(auth()->user()->can("sell.view") || auth()->user()->can("direct_sell.access") )
							<li><a href="#" class="print-invoice" data-href="{{route(\'sell.printInvoice\', [$id])}}"><i class="fa fa-print" aria-hidden="true"></i> @lang("messages.print")</a></li>
						@endif
						
						<li class="divider"></li> 
						@if($payment_status != "paid")
							@if(auth()->user()->can("sell.create") || auth()->user()->can("direct_sell.access") )
								<li><a href="{{action(\'TransactionPaymentController@addPayment\', [$id])}}" class="add_payment_modal"><i class="fa fa-money"></i> @lang("purchase.add_payment")</a></li>
							@endif
						@endif
							<li><a href="{{action(\'TransactionPaymentController@show\', [$id])}}" class="view_payment_modal"><i class="fa fa-money"></i> @lang("purchase.view_payments")</a></li>
						@can("sell.create")
							<li><a href="{{action(\'SellController@duplicateSell\', [$id])}}"><i class="fa fa-copy"></i> @lang("lang_v1.duplicate_sell")</a></li>

							<li><a href="{{action(\'SellReturnController@add\', [$id])}}"><i class="fa fa-undo"></i> @lang("lang_v1.sell_return")</a></li>
						@endcan
						@can("send_notification")
							<li><a href="#" data-href="{{action(\'NotificationController@getTemplate\', ["transaction_id" => $id,"template_for" => "new_sale"])}}" class="btn-modal" data-container=".view_modal"><i class="fa fa-envelope" aria-hidden="true"></i> @lang("lang_v1.new_sale_notification")</a></li>
						@endcan
						</ul></div>'
					)
					->removeColumn('id')
					->editColumn(
						'final_total',
						'<span class="display_currency final-total" data-currency_symbol="true" data-orig-value="{{$final_total}}">{{$final_total}}</span>'
					)
					->editColumn(
						'total_paid',
						'<span class="display_currency total-paid" data-currency_symbol="true" data-orig-value="{{$total_paid}}">{{$total_paid}}</span>'
					)
					->editColumn('transaction_date', '{{@format_date($transaction_date)}}')
					->editColumn(
						'payment_status',
						'<a href="{{ action("TransactionPaymentController@show", [$id])}}" class="view_payment_modal payment-status-label" data-orig-value="{{$payment_status}}" data-status-name="{{__(\'lang_v1.\' . $payment_status)}}"><span class="label @payment_status($payment_status)">{{__(\'lang_v1.\' . $payment_status)}}
							</span></a>'
					)
					->addColumn('total_remaining', function ($row) {
						$total_remaining =  $row->final_total - $row->total_paid;
						return '<span data-orig-value="' . $total_remaining . '" class="display_currency total-remaining" data-currency_symbol="true">' . $total_remaining . '</span>';
					})
					->setRowAttr([
						'data-href' => function ($row) {
							if (auth()->user()->can("sell.view")) {
								return  action('SellController@show', [$row->id]) ;
							} else {
								return '';
							}
						}])
					->rawColumns(['final_total', 'action', 'total_paid', 'total_remaining', 'payment_status', 'invoice_no'])
					->make(true);
			}
			return view('market::orders');
		}
		
		/**
		 * Retrieves products list.
		 *
		 * @param  string  $q
		 * @param  boolean  $check_qty
		 *
		 * @return JSON
		 */
		public function getMarketProducts()
		{
			if (request()->ajax()) {
				$term = request()->input('term', '');
				$location_id = request()->input('location_id', '');

				$check_qty = request()->input('check_qty', false);

				$price_group_id = request()->input('price_group', '');

				$business_id = request()->session()->get('user.business_id');

				$products = Product::join('variations', 'products.id', '=', 'variations.product_id');
				
				$products->where('products.business_id', $business_id)
					->where('products.supplier_id', '!=', '')
					->where('products.supplier_sku', '!=', '')
					->where('products.type', '!=', 'modifier');

				//Include search
				if (!empty($term)) {
					$products->where(function ($query) use ($term) {
							$query->where('products.name', 'like', '%' . $term .'%');
							$query->orWhere('sku', 'like', '%' . $term .'%');
							$query->orWhere('sub_sku', 'like', '%' . $term .'%');
					});
				}

				$products->select(
					'products.id as product_id',
					'products.name',
					'products.type',
					'products.enable_stock',
					'variations.id as variation_id',
					'variations.name as variation',
					'variations.sell_price_inc_tax as selling_price',
					'variations.sub_sku'
				);
				
				$result = $products->orderBy('selling_price', 'desc')->limit(10)->get();
				return json_encode($result);
			}
		}

		/**
		 * Returns the HTML row for a product in POS
		 *
		 * @param  int  $variation_id
		 * @param  int  $supplier_id
		 * @return \Illuminate\Http\Response
		 */
		public function getProductRow($variation_id, $supplier_id)
		{
			$output = [];
			try {
				$row_count = request()->get('product_row');
				$row_count = $row_count + 1;
				$is_direct_sell = false;
				if (request()->get('is_direct_sell') == 'true') {
					$is_direct_sell = true;
				}

				$business_id = request()->session()->get('user.business_id');
				$product = $this->productUtil->getDetailsFromVariation($variation_id, $business_id);
				$product->formatted_qty_available = $this->productUtil->num_f($product->qty_available);

				//Get customer group and change the price accordingly
				$customer_id = request()->get('customer_id', null);
				$cg = $this->contactUtil->getCustomerGroup($business_id, $customer_id);
				$percent = (empty($cg) || empty($cg->amount)) ? 0 : $cg->amount;
				$product->default_sell_price = $product->default_sell_price + ($percent * $product->default_sell_price / 100);
				$product->sell_price_inc_tax = $product->sell_price_inc_tax + ($percent * $product->sell_price_inc_tax / 100);

				$tax_dropdown = TaxRate::forBusinessDropdown($business_id, true, true);

				$enabled_modules = $this->transactionUtil->allModulesEnabled();

				//Get lot number dropdown if enabled
				$lot_numbers = array();
				if(request()->session()->get('business.enable_lot_number') == 1 || request()->session()->get('business.enable_product_expiry') == 1){
					$lot_number_obj = $this->transactionUtil->getLotNumbersFromVariation($variation_id, $business_id, $location_id, true);
					foreach ($lot_number_obj as $lot_number) {
						$lot_number->qty_formated = $this->productUtil->num_f($lot_number->qty_available);
						$lot_numbers[] = $lot_number;
					}
				}
				$product->lot_numbers = $lot_numbers;

				$price_group = request()->input('price_group');
				if(!empty($price_group)){
					$variation_group_prices = $this->productUtil->getVariationGroupPrice($variation_id, $price_group, $product->tax_id);
					
					if(!empty($variation_group_prices['price_inc_tax'])){
						$product->sell_price_inc_tax = $variation_group_prices['price_inc_tax'];
						$product->default_sell_price = $variation_group_prices['price_exc_tax'];  
					}
				}

				$business_details = $this->businessUtil->getDetails($business_id);
				$pos_settings = empty($business_details->pos_settings) ? $this->businessUtil->defaultPosSettings() : json_decode($business_details->pos_settings, true);

				$output['success'] = true;
				if (request()->get('type') == 'sell-return') {
					$output['html_content'] =  view('sell_return.partials.product_row')
								->with(compact('product', 'row_count', 'tax_dropdown', 'enabled_modules'))
								->render();
				} else {
					$output['html_content'] =  view('sale_pos.product_row')
								->with(compact('product', 'row_count', 'tax_dropdown', 'enabled_modules', 'pos_settings'))
								->render();
				}
				
				$output['enable_sr_no'] = $product->enable_sr_no;

				if ($this->transactionUtil->isModuleEnabled('modifiers')  && !$is_direct_sell) {
					$this_product = Product::where('business_id', $business_id)
											->find($product->product_id);
					if (count($this_product->modifier_sets) > 0) {
						$product_ms = $this_product->modifier_sets;
						$output['html_modifier'] =  view('restaurant.product_modifier_set.modifier_for_product')
						->with(compact('product_ms', 'row_count'))->render();
					}
				}
			} catch (\Exception $e) {
				\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

				$output['success'] = false;
				$output['msg'] = __('lang_v1.item_out_of_stock');
			}

			return $output;
		}
	}
