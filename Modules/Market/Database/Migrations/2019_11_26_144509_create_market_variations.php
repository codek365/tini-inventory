<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketVariations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_variations', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name');
	        $table->integer('product_id')->unsigned();
	        $table->string('sub_sku')->nullable();
	        $table->integer('supplier_id')->unsigned();
	        $table->decimal('purchase_price', 15, 2)->nullable();
	        $table->decimal('sell_price', 15, 2)->comment("Sell price")->nullable();
	        $table->timestamps();
	        $table->softDeletes();
	
	        //Indexing
	        $table->index('name');
	        $table->index('sub_sku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_variations');
    }
}
