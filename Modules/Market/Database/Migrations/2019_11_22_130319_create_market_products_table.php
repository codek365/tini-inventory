<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('market_name');
            $table->integer('supplier_id')->unsigned();
            $table->integer('customer_id')->unsigned();
	        $table->integer('product_id')->unsigned();
            $table->integer('variation_id')->unsigned();
            $table->enum('type', ['single', 'variable'])->default('single');
	        $table->string('supplier_sku');
			$table->decimal('purchase_price', 15, 2)->nullable();
            $table->decimal('sell_price', 15, 2)->nullable();
            $table->enum('status', ['selling', 'listed'])->default('listed');
	        $table->integer('created_by')->unsigned();
	        $table->timestamps();
	
	        //Indexing
	        $table->index('market_name');
	        $table->index('supplier_id');
	        $table->index('supplier_sku');
	        $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_products');
    }
}
