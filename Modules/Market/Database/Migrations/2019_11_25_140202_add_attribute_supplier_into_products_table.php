<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeSupplierIntoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('products', function (Blueprint $table) {
		    $table->tinyInteger('supplier_id')->nullable();;
		    $table->string('supplier_sku')->nullable();;
	    });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('products', function (Blueprint $table) {
		    $table->dropColumn('supplier_id');
		    $table->dropColumn('supplier_sku');
	    });
    }
}
