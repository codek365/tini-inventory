<?php

namespace Modules\Market\Entities;

use Illuminate\Database\Eloquent\Model;

class MarketProducts extends Model
{
    protected $guarded = ['id'];
}
