<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Utils\Util;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Superadmin\Entities\Package;
class PackagesController extends Controller
{
    /**
    * Constructor
    *
    * @param Util $Util
    * @return void
    */
    public function __construct(Util $Util)
    {
        $this->Util = $Util;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Util $util)
    {
        $packages = Package::all();
        return view('superadmin::packages.index', compact('packages', 'util'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (!auth()->user()->can('superadmin_packages.access')) {
            abort(403, 'Unauthorized action.');
        }

        // $business_id = request()->session()->get('user.business_id');
        
        // //Check if subscribed or not
        // if (!$this->moduleUtil->isSubscribed($business_id)) {
        //     return $this->moduleUtil->expiredResponse(action('ExpenseController@index'));
        // }

        // $business_locations = BusinessLocation::forDropdown($business_id);

        // $expense_categories = ExpenseCategory::where('business_id', $business_id)
        //                         ->pluck('name', 'id');
        // $users = User::forDropdown($business_id);
        $intervals = [
            'days' => __('superadmin::package.days'),
            'months' => __('superadmin::package.months'),
            'years' => __('superadmin::package.years'),
        ];
        // return view('expense.create')
        //     ->with(compact('expense_categories', 'business_locations', 'users'));
        return view('superadmin::packages.create')->with(compact('intervals'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('superadmin_packages.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $package_data = $request->only([ 'name', 'description', 'location_count', 'product_count', 'user_count', 'invoice_count', 'interval_count', 'interval', 'trial_days', 'price', 'sort_order']);
            // $package_data['transaction_date'] = $this->Util->uf_date($package_data['transaction_date']);
            $package_data['price'] = $this->Util->num_uf($package_data['price']);
            $user_id = $request->session()->get('user.id');
            $package_data['created_by'] = $user_id;
            $transaction = Package::create($package_data);

            $output = [
                'success' => 1,
                'msg' => __('superadmin::package.added_success')
            ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = [
                'success' => 0,
                'msg' => __('messages.something_went_wrong')
            ];
        }

        return redirect('superadmin/packages')->with('status', $output);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $package = Package::find($id);
        $intervals = [
            'days' => __('superadmin::package.days'),
            'months' => __('superadmin::package.months'),
            'years' => __('superadmin::package.years'),
        ];
        return view('superadmin::packages.edit')->with(compact('package', 'intervals'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $package_name = $request->input('package_name');
            $price = $request->input('price');
            $package_description = $request->input('package_description');

            $package = Package::findOrFail($id);
            $package->name = $package_name;
            $package->product_count = $request->input('product_count');
            $package->location_count = $request->input('location_count');
            $package->user_count = $request->input('user_count');
            $package->invoice_count = $request->input('invoice_count');
            $package->interval_count = $request->input('interval_count');
            $package->interval = $request->input('interval');
            $package->trial_days = $request->input('trial_days');
            $package->sort_order = $request->input('sort_order');
            $package->price = $price;
            $is_active = 0;
            if ($request->input('is_active') == 1) {
                $is_active = 1;
            }
            $package->is_active = $is_active;
            $package->save();

            $output = ['success' => 1,
                'msg' => __("superadmin::package.package_updated")
            ];
            
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0,
                            'msg' => __("messages.something_went_wrong")
                        ];
        }

        return redirect('superadmin/packages')->with('status', $output);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('superadmin_package.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {
                $package = Package::find($id);
                $package->delete();
                $output = ['success' => true,
                            'msg' => __("superadmin::package.deleted_success")
                        ];
                
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
                $output = ['success' => false,
                            'msg' => __("messages.something_went_wrong")
                        ];
            }

            return $output;
        }
    }
}
