<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Utils\Util;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Superadmin\Entities\Subscription;
use Yajra\DataTables\Facades\DataTables;
use Modules\Superadmin\Entities\Package;
use Illuminate\Support\Facades\Auth;
class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {   
        if (!auth()->user()->can('superadmin.access')) {
            abort(403, 'Unauthorized action.');
        }
        if (request()->ajax()) {
            $subscriptions = Subscription::leftJoin('business as b', 'subscriptions.business_id', '=', 'b.id')
            ->leftJoin('packages as p', 'subscriptions.package_id', '=', 'p.id')
            ->select(['subscriptions.id', 'b.name as business_name', 'p.name as package_name', 'end_date', 'subscriptions.start_date', 'trial_end_date', 'package_price', 'paid_via', 'payment_transaction_id', 'status']);
            return Datatables::of($subscriptions)
                ->addColumn(
                    'action',
                    '<a class="btn btn-xs btn-primary edit_subscription_button" data-href="{{action(\'\Modules\Superadmin\Http\Controllers\SubscriptionsController@edit\', [$id])}}"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</a>'
                )
                ->editColumn('start_date', '{{@format_date($start_date)}}')
                ->editColumn('end_date', '{{@format_date($end_date)}}')
                ->editColumn('trial_end_date', '{{@format_date($trial_end_date)}}')
                ->removeColumn('id')
                ->rawColumns(['action'])
                ->make(true);
        }
       
        return view('superadmin::subscription.index');
    }

    public function mySubscription(Util $util)
    {
        $packages = Package::where('is_active', '1')->get();
        return view('superadmin::subscription.my', compact('packages', 'util'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('superadmin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('superadmin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $subscription = Subscription::find($id);
        $status = [
            'approved' => __('superadmin::subscription.approved'),
            'waiting' => __('superadmin::subscription.waiting'),
            'declined' => __('superadmin::subscription.declined'),
        ];
        return view('superadmin::subscription.edit')->with(compact('subscription', 'status'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('superadmin_subscription.update')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {
                $input = $request->only(['status','start_date', 'end_date', 'trial_end_date']);

                $subscription = Subscription::findOrFail($id);
                $subscription->status = $input['status'];
                $subscription->start_date = \Carbon::createFromFormat('d-m-Y', $input['start_date'])->format('Y-m-d');
                $subscription->trial_end_date = \Carbon::createFromFormat('d-m-Y', $input['trial_end_date'])->format('Y-m-d');
                $subscription->end_date = \Carbon::createFromFormat('d-m-Y', $input['end_date'])->format('Y-m-d');
                $subscription->save();

                $output = ['success' => true,
                            'msg' => __("superadmin::subscription.updated_success")
                            ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
                $output = ['success' => false,
                            'msg' => __("messages.something_went_wrong")
                        ];
            }

            return $output;
        }
    }
    
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function pay(Util $util, $package_id)
    {
        if(!request()->session()->has('user')){
            $user = Auth::user();
            $user_id = $user->id;
            $business_id = $user->business_id;
        }else{
            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');
        }

        $package = Package::find($package_id);
        $mySubscriptions = Subscription::where('business_id', $business_id)
        ->where('package_id', $package_id)
        ->get();
        $subscription_start = \Carbon::today()->subDay(2)->toDateString();
        $subscription_trial = \Carbon::today()->addDays(8)->toDateString();
        $subscription_end = \Carbon::today()->addDays(28)->toDateString();

        $subscription = [
            'business_id' => $business_id,
            'package_id' => $package_id,
            'start_date' => $subscription_start,
            'trial_end_date' => $subscription_trial,
            'end_date' => 0,
            'package_price' => '0',
            'package_details' => '{"location_count":2,"user_count":2,"product_count":10,"invoice_count":10,"name":"Trial"}',
            'paid_via' => 'free',
            'created_id' => $user_id,
            'payment_transaction_id' => '',
            'status' => 'approved',
            'deleted_at' => null
        ];

        if($package_id == env('PACKAGE_FREE_ID', 1)){

            if($mySubscriptions->isEmpty()){
                Subscription::create($subscription);
                $output = ['success' => 1,
                    'msg' => __('superadmin::subscription.add_free_success')
                ];
            }else{
                $output = ['success' => 1,
                    'msg' => __('superadmin::subscription.is_free')
                ];
            }
            
            return redirect()->route('subscription')->with('status', $output);
        }
        
        if(!empty($package)){
            return view('superadmin::subscription.pay')->with(compact('package', 'util'));
        }
        
    }

    public function confirm(Request $request, $package_id)
    {
        
    }
}
