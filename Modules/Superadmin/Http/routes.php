<?php

Route::group(['middleware' => 'web', 'prefix' => 'superadmin', 'namespace' => 'Modules\Superadmin\Http\Controllers'], function()
{
    Route::get('/', 'SuperadminController@index');
    Route::get('/install', 'SuperadminController@install');
    // Route::get('/business', 'BusinessController@index');
    Route::resource('/packages', 'PackagesController');
    // Route::get('/packages', 'PackagesController@index');
    // Route::get('/packages/create', 'PackagesController@create');
    // Route::get('/packages/{id}/edit', 'PackagesController@edit');
    // Route::put('packages/{id}', 'PackagesController@update');
    // Route::get('/packages/{id}/destroy', 'PackagesController@destroy');
    Route::get('/subscription', 'SubscriptionController@index');
    Route::get('/subscription/{id}/edit', 'SubscriptionController@edit');
    Route::get('/subscription/{id}/pay', 'SubscriptionController@pay')->name('register-pay');
    Route::get('/subscription/{id}/confirm', 'SubscriptionController@confirm');
    Route::get('/subscription/{id}/destroy', 'SubscriptionController@destroy');
    Route::put('subscription/{id}', 'SubscriptionController@update');
    Route::get('/subscription/create', 'SubscriptionController@create');
    Route::get('pricing', 'PricingController@index')->name('pricing');
});

Route::group(['middleware' => 'web', 'prefix' => '', 'namespace' => 'Modules\Superadmin\Http\Controllers'], function()
{
    Route::get('/subscription', 'SubscriptionController@mySubscription')->name('subscription');
    Route::get('/pricing', 'PricingController@index');
});
// Route::resource('packages', 'PackagesController');
