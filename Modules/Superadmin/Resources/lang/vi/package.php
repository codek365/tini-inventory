<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Superadmin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during superadmin for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Gói đăng ký',
    'all_packages' => 'Tất cả các gói đăng ký',
    'active' => 'Đã kích hoạt',
    'inactive' => 'Ngừng kích hoạt',
    'locations' => 'Chi nhánh',
    'users' => 'Người dùng',
    'products' => 'Sản phẩm',
    'invoices' => 'Hóa đơn',
    'trial_days' => 'Ngày dùng thử',
    'free_for' => 'Miễn phí ',
    'edit' => 'Sửa gói',
    'add' => 'Thêm gói',
    'remove' => 'Xóa gói',
    'create' => 'Thêm mới gói đăng ký',
    'save' => 'Lưu',
    'package_name' => 'Tên gói',
    'package_description' => 'Mô tả',
    'location_count' => 'Giới hạn chi nhánh',
    'user_count' => 'Giới hạn thành viên',
    'product_count' => 'Giới hạn sản phẩm',
    'invoice_count' => 'Giới hạn hóa đơn',
    'interval_count' => 'Thời hạn',
    'interval' => 'Định kỳ',
    'price' => 'Giá',
    'days' => 'Ngày',
    'months' => 'Tháng',
    'years' => 'Năm',
    'null_count' => 'Đặt 0 để không giới hạn',
    'pricing' => 'Bảng giá',
    'sort_order' => 'Vị trí sắp xếp',
    'is_active' => 'Đã kích hoạt',
    'package_updated' => 'Cập nhật gói thành công',
    'deleted_success' => 'Xóa gói đăng ký thành công',
    'added_success' => 'Thêm gói đăng ký thành công'

];