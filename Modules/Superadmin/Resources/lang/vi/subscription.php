<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Superadmin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during superadmin for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Đăng ký mua',
    'all_your_subscriptions' => 'Tất cả các đăng ký mua',
    'manager_subcription' => 'Quản lý đăng ký mua',
    'inactive' => 'Ngừng kích hoạt',
    'locations' => 'Chi nhánh',
    'users' => 'Người dùng',
    'products' => 'Sản phẩm',
    'invoices' => 'Hóa đơn',
    'trial_days' => 'Ngày dùng thử',
    'free_for' => 'Miễn phí ',
    'edit' => 'Sửa gói',
    'add' => 'Thêm gói',
    'remove' => 'Xóa gói',
    'save' => 'Lưu',
    'pay' => 'Thanh toán',
    'package_name' => 'Tên gói',
    'business_name' => 'Doanh nghiệp',
    'start_date' => 'Bắt đầu',
    'end_date' => 'Kết thúc',
    'trial_end_date' => 'Hết dùng thử',
    'payment_method' => 'Thanh toán',
    'transaction_id' => 'Mã thanh toán',
    'status' => 'Trạng thái',
    'trial_days' => 'Thời hạn dùng thử',
    'price' => 'Giá',
    'approved' => 'Đã kích hoạt',
    'waiting' => 'Đang chờ xử lý',
    'declined' => 'Đã hết hạn',
    'sort_order' => 'Vị trí sắp xếp',
    'is_active' => 'Đã kích hoạt',
    'add_success' => 'Thêm đăng ký mua thành công',
    'add_free_success' => 'Đăng ký gói miễn phí thành công',
    'updated_success' => 'Cập nhật đăng ký mua thành công',
    'is_free' => 'Bạn đang sử dụng gói miễn phí'

];