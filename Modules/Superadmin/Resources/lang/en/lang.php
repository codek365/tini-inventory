<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Superadmin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during superadmin for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'Super Admin',
    'packages' => 'Packages',
    'subscription' => 'Subscription',    
    'subscription_expired_toastr' => 'No active subscription found for your business.'
];