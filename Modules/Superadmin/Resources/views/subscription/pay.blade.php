@extends('layouts.app')
@section('title', __('expense.add_expense'))
@section('content')
<!-- Main content -->
<section class="content">

	<div class="box box-success">
        <div class="box-header with-border">
            <h2 class="box-title">@lang('superadmin::subscription.pay')</h2>
        </div>
        <div class="box-body">
			<div class="col-md-4">
				<div class="box box-success hvr-grow-shadow">
					<div class="box-header with-border text-center">
						<h2 class="box-title">
						{{ $package->name }} <span class="package_price">{{$util->num_f($package->price, true, 0)}}</span> / {{$package->interval_count}} {{$package->interval}}
						</h2>
					</div>
					<div class="box-body text-center">
							<i class="fa fa-check text-success"></i> {{($package->location_count == 0) ? 'Không giới hạn' : $package->location_count}} @lang('superadmin::package.locations') <hr>
							<i class="fa fa-check text-success"></i> {{($package->user_count == 0) ? 'Không giới hạn' : $package->user_count}} @lang('superadmin::package.users') <hr>
							<i class="fa fa-check text-success"></i> {{($package->product_count == 0) ? 'Không giới hạn' : $package->product_count}} @lang('superadmin::package.products') <hr>
							<i class="fa fa-check text-success"></i> {{$package->trial_days}} @lang('superadmin::package.trial_days') <hr>
						
					</div>
					<div class="box-footer text-center">
						
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

@endsection
