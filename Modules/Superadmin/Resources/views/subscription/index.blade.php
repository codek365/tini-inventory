@extends('layouts.app')
@section('title', __('superadmin::subscription.title'))

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang( 'superadmin::subscription.title' )
        <small>@lang( 'superadmin::subscription.manager_subcription' )</small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">@lang( 'superadmin::subscription.all_your_subscriptions' )</h3>
        </div>
        <div class="box-body">
            @can('superadmin_subscription.view')
                <div class="table-responsive">
            	<table class="table table-bordered table-striped" id="superadmin_subscription_table">
            		<thead>
            			<tr>
            				<th>@lang( 'superadmin::subscription.business_name' )</th>
            				<th>@lang( 'superadmin::subscription.package_name' )</th>
            				<th>@lang( 'superadmin::subscription.status' )</th>
            				<th>@lang( 'superadmin::subscription.start_date' )</th>
                            <th>@lang( 'superadmin::subscription.trial_end_date' )</th>
            				<th>@lang( 'superadmin::subscription.end_date' )</th>
            				<th>@lang( 'superadmin::subscription.price' )</th>
            				<th>@lang( 'superadmin::subscription.payment_method' )</th>
            				<th>@lang( 'superadmin::subscription.transaction_id' )</th>
            				<th>@lang( 'messages.action' )</th>
            			</tr>
            		</thead>
            	</table>
                </div>
            @endcan
        </div>
    </div>

    <div class="modal fade subscription_modal" tabindex="-1" role="dialog" 
    	aria-labelledby="gridSystemModalLabel">
    </div>

</section>
<!-- /.content -->
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        // superadmin_subscription_table
        var superadmin_subscription_table = $('#superadmin_subscription_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/superadmin/subscriptions',
            columnDefs:[{
                    "targets": 9,
                    "orderable": false,
                    "searchable": false
                }],
            columns: [
                { data: 'business_name', name: 'business_name'  },
                { data: 'package_name', name: 'package_name'},
                { data: 'status', name: 'status'},
                { data: 'start_date', name: 'start_date'},
                { data: 'trial_end_date', name: 'trial_end_date'},
                { data: 'end_date', name: 'end_date'},
                { data: 'package_price', name: 'package_price'},
                { data: 'paid_via', name: 'paid_via'},
                { data: 'payment_transaction_id', name: 'payment_transaction_id'},
                { data: 'action', name: 'action'}
            ],
            "fnDrawCallback": function (oSettings) {
                __currency_convert_recursively($('#superadmin_subscription_table'), true);
            }
        });

        $(document).on('click', 'a.edit_subscription_button', function(){

            $( "div.subscription_modal" ).load( $(this).data('href'), function(){

                $(this).modal('show');
                $('.subscription_modal .datepicker').datepicker({
                    autoclose: true,
                    format:datepicker_date_format
                });
                
                $('form#edit_subscription_form').submit(function(e){
                    e.preventDefault();
                    var data = $(this).serialize();

                    $.ajax({
                        method: "POST",
                        url: $(this).attr("action"),
                        dataType: "json",
                        data: data,
                        success: function(result){
                            if(result.success == true){
                                $('div.subscription_modal').modal('hide');
                                toastr.success(result.msg);
                                superadmin_subscription_table.ajax.reload();
                            } else {
                                toastr.error(result.msg);
                            }
                        }
                    });
                });
            });
        });
});
</script>
@endsection