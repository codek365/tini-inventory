<div class="modal-dialog" role="document">
	<div class="modal-content">

		{!! Form::open(['url' => action('\Modules\Superadmin\Http\Controllers\SubscriptionController@update', [$subscription->id]), 'method' => 'PUT', 'id' => 'edit_subscription_form' ]) !!}

		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">@lang( 'superadmin::subscription.edit' )</h4>
		</div>

		<div class="modal-body">
			<div class="form-group">
				{!! Form::label('status', __('superadmin::subscription.status').' :') !!}
				{!! Form::select('status', $status, $subscription->status, ['class' => 'form-control', 'id' => 'subscription_status']); !!}
			</div>
			
			<div class="form-group">
				{!! Form::label('start_date', __( 'superadmin::subscription.start_date' )) !!}
				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</span>
					{!! Form::text('start_date', @format_date($subscription->start_date), ['class' => 'form-control datepicker', 'readonly']); !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('trial_end_date', __( 'superadmin::subscription.trial_end_date' )) !!}
				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</span>
					{!! Form::text('trial_end_date', @format_date($subscription->trial_end_date), ['class' => 'form-control datepicker', 'readonly']); !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('end_date', __( 'superadmin::subscription.end_date' )) !!}
				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</span>
					{!! Form::text('end_date', @format_date($subscription->end_date), ['class' => 'form-control datepicker', 'readonly']); !!}
				</div>
			</div>

		</div>

		<div class="modal-footer">
		<button type="submit" class="btn btn-primary">@lang( 'messages.update' )</button>
		<button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
		</div>

		{!! Form::close() !!}

	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->