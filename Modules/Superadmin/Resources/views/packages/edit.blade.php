@extends('layouts.app')
@section('title', __('superadmin::package.edit'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('superadmin::package.edit')</h1><small>@lang('superadmin::package.null_count')</small>
</section>

<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('\Modules\Superadmin\Http\Controllers\PackagesController@update', [$package->id]), 'method' => 'PUT', 'id' => 'package_form' ]) !!}
	<div class="box box-solid">
		<div class="box-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('package_name', __('superadmin::package.package_name').' :*') !!}
					{!! Form::text('package_name', $package->name, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('package_description', __('superadmin::package.package_description').' :') !!}
					{!! Form::text('package_description', $package->description, ['class' => 'form-control', 'required']); !!}
				</div>
			</div> 
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('location_count', __('superadmin::package.location_count').' :') !!}
					{!! Form::text('location_count', $package->location_count, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('product_count', __('superadmin::package.product_count').' :') !!}
					{!! Form::text('product_count', $package->product_count, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('user_count', __('superadmin::package.user_count').' :') !!}
					{!! Form::text('user_count', $package->user_count, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('invoice_count', __('superadmin::package.invoice_count').' :') !!}
					{!! Form::text('invoice_count', $package->invoice_count, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('interval_count', __('superadmin::package.interval_count').' :') !!}
					{!! Form::text('interval_count', $package->interval_count, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('interval', __('superadmin::package.interval').' :') !!}
					{!! Form::select('interval', $intervals, $package->interval, ['class' => 'form-control', 'id' => 'package_interval','placeholder' => __('messages.please_select'), 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('trial_days', __('superadmin::package.trial_days').' :') !!}
					{!! Form::text('trial_days', $package->trial_days, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('price', __('superadmin::package.price').' :') !!}
					{!! Form::text('price', $package->price, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('sort_order', __('superadmin::package.sort_order').' :') !!}
					{!! Form::text('sort_order', $package->sort_order, ['class' => 'form-control', 'required']); !!}
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="checkbox">
					<label>
						{!! Form::checkbox('is_active', 1, $package->is_active , 
						[ 'class' => 'input-icheck']); !!} {{ __( 'superadmin::package.is_active' ) }}
					</label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
			<button type="submit" class="btn btn-primary pull-right">@lang('messages.update')</button>
			</div>
		</div>
		</div>
	</div> <!--box end-->

{!! Form::close() !!}
</section>
@endsection