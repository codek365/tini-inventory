@extends('layouts.app')
@section('title', __('superadmin::package.title'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('superadmin::package.title')
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

	<div class="box box-success">
        <div class="box-header">
        	<h3 class="box-title">@lang('superadmin::package.all_packages')</h3>
            <div class="box-tools">
                <a class="btn btn-block btn-primary" href="{{action('\Modules\Superadmin\Http\Controllers\PackagesController@create')}}">
                <i class="fa fa-plus"></i> @lang('messages.add')</a>
            </div>
        </div>
        <div class="box-body">
            @foreach($packages as $package)
                <div class="col-md-4">
                    <div class="box box-success hvr-grow-shadow">
                        <div class="box-header with-border text-center">
                            <h2 class="box-title">
                            {{ $package->name }}
                            <div class="row">
                                <span class="badge {{ $package->is_active ? 'bg-green' : 'bg-red'  }}">
                                @if ($package->is_active === 1)
                                    @lang('superadmin::package.active')
                                @else
                                    @lang('superadmin::package.inactive')
                                @endif
                                </span>
								<a href="{{action('\Modules\Superadmin\Http\Controllers\PackagesController@edit', [$package->id])}}" class="btn btn-box-tool" title="edit"><i class="fa fa-edit"></i></a>
								<a href="{{action('\Modules\Superadmin\Http\Controllers\PackagesController@destroy', [$package->id])}}" class="btn btn-box-tool link_confirmation" title="delete"><i class="fa fa-trash"></i></a>
							</div>
                            </h2>
                        </div>
                        <div class="box-body text-center">
                            <ul class="nav nav-stacked">
                                <li class="text-center">{{($package->location_count == 0) ? 'Không giới hạn' : $package->location_count}} @lang('superadmin::package.locations') </li>
                                <li class="text-center">{{($package->user_count == 0) ? 'Không giới hạn' : $package->user_count}} @lang('superadmin::package.users') </li>
                                <li class="text-center">{{($package->product_count == 0) ? 'Không giới hạn' : $package->product_count}} @lang('superadmin::package.products') </li>
                                <li class="text-center">{{$package->trial_days}} @lang('superadmin::package.trial_days') </li>
                            </ul>
                            <h3 class="text-center">
                                @if ($package->price == 0)
                                    @lang('superadmin::package.free_for') {{$package->interval_count}} @lang('superadmin::package.months')
                                @else
                                    <span class="package_price">{{$util->num_f($package->price, true, 0)}}</span> / {{$package->interval_count}} @lang('superadmin::package.months')
                                @endif
                            </h3>
                        </div>
                        <div class="box-footer text-center">
                            {{$package->description}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</section>
<!-- /.content -->
@endsection
@section('javascript')
<script src="{{ Module::asset('superadmin:js/superadmin.js') }}"></script>
@endsection