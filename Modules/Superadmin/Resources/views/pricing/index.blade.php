@extends('layouts.auth')
@section('title', __('superadmin::package.title'))

@section('content')
<!-- Main content -->
<section class="content">

	<div class="box box-success">
        <div class="box-header with-border text-center">
            <h2 class="box-title">@lang('superadmin::package.pricing')</h2>
        </div>
        <div class="box-body">
            @foreach($packages as $package)
                <div class="col-md-4">
                    <div class="box box-success hvr-grow-shadow">
                        <div class="box-header with-border text-center">
                            <h2 class="box-title">
                            {{ $package->name }}
                            </h2>
                        </div>
                        <div class="box-body text-center">
                                <i class="fa fa-check text-success"></i> {{($package->location_count == 0) ? 'Không giới hạn' : $package->location_count}} @lang('superadmin::package.locations') <hr>
                                <i class="fa fa-check text-success"></i> {{($package->user_count == 0) ? 'Không giới hạn' : $package->user_count}} @lang('superadmin::package.users') <hr>
                                <i class="fa fa-check text-success"></i> {{($package->product_count == 0) ? 'Không giới hạn' : $package->product_count}} @lang('superadmin::package.products') <hr>
                                <i class="fa fa-check text-success"></i> {{$package->trial_days}} @lang('superadmin::package.trial_days') <hr>
                            <h3 class="text-center">
                                @if ($package->price == 0)
                                    @lang('superadmin::package.free_for') {{$package->interval_count}} {{$package->interval}}
                                @else
                                    <span class="package_price">{{$util->num_f($package->price, true, 0)}}</span> / {{$package->interval_count}} {{$package->interval}}
                                @endif
                            </h3>
                        </div>
                        <div class="box-footer text-center">
                            @if ($package->price == 0)
                                <a href="{{action('BusinessController@getRegister', ['package' => $package->id])}}" class="btn btn-block btn-success">
                                Dùng thử miễn phí</a>
                            @else
                                <a href="{{action('BusinessController@getRegister', ['package' => $package->id])}}" class="btn btn-block btn-success">
                                Đăng ký ngay</a>
                            @endif
                                
                            {{$package->description}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</section>
<!-- /.content -->
@endsection
@section('javascript')
<script src="{{ Module::asset('superadmin:js/superadmin.js') }}"></script>
@endsection