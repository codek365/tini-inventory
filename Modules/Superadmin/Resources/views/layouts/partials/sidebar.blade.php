@if(in_array(Auth::User()->username, explode(',', env('ADMINISTRATOR_USERNAMES')))) 
<li class="treeview {{$request->segment(1) == 'superadmin' ? 'active active-sub' : '' }}">
    <a href="#"><i class="fa fa-user-secret"></i> <span>@lang('superadmin::lang.title')</span>
        <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ $request->segment(1) == 'superadmin' ? 'active' : '' }}"><a href="{{action('\Modules\Superadmin\Http\Controllers\SuperadminController@index')}}"><i class="fa fa-user-secret"></i> @lang('superadmin::lang.title')</a></li>
        <li class="{{ $request->segment(1) == 'superadmin' && $request->segment(2) == 'business' ? 'active' : '' }}"><a href=""><i class="fa fa-list"></i>@lang('superadmin::lang.business')</a></li>
        <li class="{{ $request->segment(1) == 'superadmin' && $request->segment(2) == 'subscriptions' ? 'active' : '' }}"><a href="{{action('\Modules\Superadmin\Http\Controllers\SubscriptionController@index')}}"><i class="fa fa-refresh"></i> @lang('superadmin::lang.subscriptions')</a></li> 
        <li class="{{ $request->segment(1) == 'superadmin' && $request->segment(2) == 'packages' ? 'active' : '' }}"><a href="{{action('\Modules\Superadmin\Http\Controllers\PackagesController@index')}}"><i class="fa fa-credit-card"></i> @lang('superadmin::lang.packages')</a></li>
    </ul>
</li>
@endif