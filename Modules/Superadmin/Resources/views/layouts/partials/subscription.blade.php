<li class="{{ $request->segment(1) == 'superadmin' ? 'active' : '' }}">
    <a href="{{action('\Modules\Superadmin\Http\Controllers\SubscriptionController@mySubscription')}}">
        <i class="fa fa-dashboard"></i> <span>
        @lang('superadmin::subscription.title')</span>
    </a>
</li>