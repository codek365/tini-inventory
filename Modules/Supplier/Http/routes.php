<?php
Route::group(['middleware' => ['web', 'supplier'], 'prefix' => 'supplier', 'namespace' => 'Modules\Supplier\Http\Controllers'], function()
{
    Route::get('/', 'SupplierController@index');
    
	Route::post('/products/delete', 'ProductController@massDestroy');
	Route::get('/products/view/{id}', 'ProductController@view');
	Route::get('/products/list', 'ProductController@getProducts');
	Route::post('/products/check_product_sku', 'ProductController@checkProductSku');
	Route::post('/products/save_quick_product', 'ProductController@saveQuickProduct');
	
	Route::resource('products', 'ProductController');
	
	// Route::get('/order/get_product_row/{variation_id}/{location_id}', 'OrderController@getProductRow');
	// Route::post('/order/get_payment_row', 'OrderController@getPaymentRow');
	// Route::get('/order/get-recent-transactions', 'OrderController@getRecentTransactions');
	// Route::get('/order/get-product-suggestion', 'OrderController@getProductSuggestion');
	// Route::resource('order', 'OrderController');
});
