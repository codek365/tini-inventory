<?php

namespace Modules\Supplier\Http\Controllers;

use App\Brands;
use App\Business;
use App\BusinessLocation;
use App\Category;
use App\Product;
use App\Utils\ModuleUtil;
use App\Utils\ProductUtil;
use App\Utils\TransactionUtil;
use Illuminate\Support\Facades\DB;
use Modules\Supplier\Entities\SupplierProducts;
use App\SellingPriceGroup;
use App\TaxRate;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
	/**
	 * Constructor
	 *
	 * @param ProductUtils $product
	 * @return void
	 */
	public function __construct(ProductUtil $productUtil, ModuleUtil $moduleUtil, TransactionUtil $transactionUtil)
	{
		$this->productUtil = $productUtil;
		$this->moduleUtil = $moduleUtil;
		$this->transactionUtil = $transactionUtil;
		if (!$this->transactionUtil->isModuleEnabled('suppliers')) {
			return redirect('/home');
		}
	}
    /**
     * Display a listing of the resource.
     * @return Response
     */
	public function index()
	{
		
		$business_id = request()->session()->get('user.business_id');
		$selling_price_group_count = SellingPriceGroup::countSellingPriceGroups($business_id);
		
		if (request()->ajax()) {
			$products = SupplierProducts::leftJoin('brands', 'supplier_products.brand_id', '=', 'brands.id')
				->leftJoin('units', 'supplier_products.unit_id', '=', 'units.id')
				->leftJoin('categories as c1', 'supplier_products.category_id', '=', 'c1.id')
				->leftJoin('categories as c2', 'supplier_products.sub_category_id', '=', 'c2.id')
				->leftJoin('tax_rates', 'supplier_products.tax', '=', 'tax_rates.id')
				->where('supplier_products.type', '!=', 'modifier')
				->select(
					'supplier_products.id',
					'supplier_products.supplier_id',
					'supplier_products.name as product',
					'supplier_products.type',
					'c1.name as category',
					'c2.name as sub_category',
					'units.actual_name as unit',
					'brands.name as brand',
					'tax_rates.name as tax',
					'supplier_products.sku',
					'supplier_products.image'
				);
			return Datatables::of($products)
				->addColumn(
					'action', function($row) use($selling_price_group_count){
					$html =
						'<div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">'. __("messages.actions") . '<span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="' . action('LabelsController@show') . '?product_id=' . $row->id . '" data-toggle="tooltip" title="Print Barcode/Label"><i class="fa fa-barcode"></i> ' . __('barcode.labels') . '</a></li>';
					
					if(auth()->user()->can('product.view')){
						$html .=
							'<li><a href="' . action('ProductController@view', [$row->id]) . '" class="view-product"><i class="fa fa-eye"></i> ' . __("messages.view") . '</a></li>';
					}
					
					if(auth()->user()->can('product.update')){
						$html .=
							'<li><a href="' . action('ProductController@edit', [$row->id]) . '"><i class="glyphicon glyphicon-edit"></i> ' . __("messages.edit") . '</a></li>';
					}
					
					if(auth()->user()->can('product.delete')){
						$html .=
							'<li><a href="' . action('ProductController@destroy', [$row->id]) . '" class="delete-product"><i class="fa fa-trash"></i> ' . __("messages.delete") . '</a></li>';
					}
					
					$html .= '<li class="divider"></li>';
					
					if(auth()->user()->can('product.create')){
						$html .=
							'<li><a href="#" data-href="' . action('OpeningStockController@add', ['product_id' => $row->id]) . '" class="add-opening-stock"><i class="fa fa-database"></i> ' . __("lang_v1.add_edit_opening_stock") . '</a></li>';
						if($selling_price_group_count > 0){
							$html .=
								'<li><a href="' . action('ProductController@addSellingPrices', [$row->id]) . '"><i class="fa fa-money"></i> ' . __("lang_v1.add_selling_price_group_prices") . '</a></li>';
						}
						
						$html .=
							'<li><a href="' . action('ProductController@create', ["d" => $row->id]) . '"><i class="fa fa-copy"></i> ' . __("lang_v1.duplicate_product") . '</a></li>';
					}
					
					$html .= '</ul></div>';
					
					return $html;
				}
				)
				->editColumn('image', function ($row) {
					return '<div style="display: flex;"><img src="' . $row->image_url . '" alt="Product image" class="product-thumbnail-small"></div>';
				})
				->addColumn('mass_delete', function($row){
					if (auth()->user()->can("product.delete")) {
						return  '<input type="checkbox" class="row-select" value="' . $row->id .'">' ;
					} else {
						return '';
					}
				})
				->setRowAttr([
					'data-href' => function ($row) {
						if (auth()->user()->can("product.view")) {
							return  action('\Modules\Supplier\Http\Controllers\ProductController@view', [$row->id]) ;
						} else {
							return '';
						}
					}])
				->rawColumns(['action', 'image', 'mass_delete'])
				->make(true);
		}
		
		
		return view('supplier::product.index');
	}

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
	public function create()
	{
		
		$business_id = request()->session()->get('user.business_id');
	
		$categories = Category::where('business_id', $business_id)
			->where('parent_id', 0)
			->pluck('name', 'id');
		$brands = Brands::where('business_id', $business_id)
			->pluck('name', 'id');
		$units = Unit::where('business_id', $business_id)
			->pluck('short_name', 'id');
		
		$tax_dropdown = TaxRate::forBusinessDropdown($business_id, true, true);
		$taxes = $tax_dropdown['tax_rates'];
		$tax_attributes = $tax_dropdown['attributes'];
		
		
		$default_profit_percent = Business::where('id', $business_id)->value('default_profit_percent');
		
		//Get all business locations
		$business_locations = BusinessLocation::forDropdown($business_id);
		
		//Duplicate product
		$duplicate_product = null;
		
		$sub_categories = array();
		
		
		$selling_price_group_count = SellingPriceGroup::countSellingPriceGroups($business_id);
		
		return view('supplier::product.create')
			->with(compact('categories', 'brands', 'units', 'taxes', 'default_profit_percent', 'tax_attributes', 'business_locations', 'duplicate_product', 'sub_categories', 'selling_price_group_count'));
	}

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
	public function store(Request $request)
	{
		
		try {
			$supplier_id = $request->session()->get('user.supplier_id');
			$business_id = request()->session()->get('user.business_id');
			$product_details = $request->only(['name', 'brand_id', 'unit_id', 'category_id', 'tax', 'type', 'barcode_type', 'sku', 'alert_quantity', 'tax_type', 'weight']);
			$product_details['supplier_id'] = $business_id;
			$product_details['created_by'] = $request->session()->get('user.id');
			
			if (!empty($request->input('enable_stock')) &&  $request->input('enable_stock') == 1) {
				$product_details['enable_stock'] = 1 ;
			}
			
			if (!empty($request->input('sub_category_id'))) {
				$product_details['sub_category_id'] = $request->input('sub_category_id') ;
			}
			
			if (empty($product_details['sku'])) {
				$product_details['sku'] = ' ';
			}
			
			$expiry_enabled = $request->session()->get('business.enable_product_expiry');
			if (!empty($request->input('expiry_period_type')) && !empty($request->input('expiry_period')) && !empty($expiry_enabled) && ($product_details['enable_stock'] == 1)) {
				$product_details['expiry_period_type'] = $request->input('expiry_period_type');
				$product_details['expiry_period'] = $this->productUtil->num_uf($request->input('expiry_period'));
			}
			
			if (!empty($request->input('enable_sr_no')) &&  $request->input('enable_sr_no') == 1) {
				$product_details['enable_sr_no'] = 1 ;
			}
			
			//upload image
			if ($request->hasFile('image') && $request->file('image')->isValid()) {
				if ($request->image->getSize() <= config('constants.image_size_limit')) {
					$new_file_name = time() . '_' . $request->image->getClientOriginalName();
					$image_path = sprintf(config('constants.suppliers_img_path_format'),$business_id);
					$path = $request->image->storeAs($image_path, $new_file_name);
					if ($path) {
						$product_details['image'] = $new_file_name;
					}
				}
			}
			
			DB::beginTransaction();
			$product = SupplierProducts::create($product_details);
			if (empty(trim($request->input('sku')))) {
				$sku = $this->productUtil->generateProductSku($product->id);
				$product->sku = $sku;
				$product->save();
			}
			if ($product->type == 'single') {
				$this->productUtil->createSingleProductVariation($product, $product->sku, $request->input('single_dpp'), $request->input('single_dpp_inc_tax'), $request->input('profit_percent'), $request->input('single_dsp'), $request->input('single_dsp_inc_tax'));
			} elseif ($product->type == 'variable') {
				if (!empty($request->input('product_variation'))) {
					$input_variations = $request->input('product_variation');
					$this->productUtil->createVariableProductVariations($product, $input_variations, 'supplier_product_id');
				}
			}
		
			DB::commit();
			$output = ['success' => 1,
				'msg' => __('product.product_added_success')
			];
		} catch (\Exception $e) {
			DB::rollBack();
			\Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
			
			$output = ['success' => 0,
				'msg' => $e->getMessage()
			];
			return redirect('supplier/products')->with('status', $output);
		}
		
//		if ($request->input('submit_type') == 'submit_n_add_opening_stock') {
//			return redirect()->action(
//				'OpeningStockController@add',
//				['product_id' => $product->id]
//			);
//		} else if($request->input('submit_type') == 'submit_n_add_selling_prices'){
//			return redirect()->action(
//				'\Modules\Supplier\Http\Controllers\ProductController@addSellingPrices',
//				[$product->id]
//			);
//		} else if($request->input('submit_type') == 'save_n_add_another'){
//			return redirect()->action(
//				'\Modules\Supplier\Http\Controllers\ProductController@create'
//			)->with('status', $output);
//		}
		
		return redirect('supplier/products')->with('status', $output);
	}

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('supplier::product.show');
    }
	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Product  $product
	 * @return \Illuminate\Http\Response
	 */
	public function view($id)
	{
		
		$business_id = request()->session()->get('user.business_id');
		
		$product = SupplierProducts::where('id', $id)
			->with(['brand', 'unit', 'category', 'sub_category', 'product_tax', 'variations', 'variations.product_variation', 'variations.group_prices'])
			->first();
		
		$price_groups = SellingPriceGroup::where('business_id', $business_id)->pluck('name', 'id');
		
		$allowed_group_prices = [];
		foreach ($price_groups as $key => $value) {
			if(auth()->user()->can('selling_price_group.' . $key)){
				$allowed_group_prices[$key] = $value;
			}
		}
		
		$group_price_details = [];
		
		foreach ($product->variations as $variation) {
			foreach ($variation->group_prices as $group_price) {
				$group_price_details[$variation->id][$group_price->price_group_id] = $group_price->price_inc_tax;
			}
		}
		
		
		return view('supplier::product.view-modal')->with(compact('product', 'allowed_group_prices',
			'group_price_details'));
	}

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('supplier::product.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
