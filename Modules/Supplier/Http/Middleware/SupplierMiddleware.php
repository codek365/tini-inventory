<?php

namespace Modules\Supplier\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Utils\TransactionUtil;

class SupplierMiddleware
{
	public function __construct(TransactionUtil $transactionUtil)
	{
		$this->transactionUtil = $transactionUtil;
	}
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
	    if (!$this->transactionUtil->isModuleEnabled('suppliers')) {
		    return redirect('/home');
	    }
	
        return $next($request);
    }
}
