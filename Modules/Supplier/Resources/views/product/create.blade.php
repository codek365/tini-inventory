@extends('supplier::layouts.master')
@section('title', __('product.add_new_product'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('product.add_new_product')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{action('HomeController@index')}}">
                <i class="fa fa-dashboard"></i> <span>@lang('home.home')</span>
            </a>
        <li>
            <a href="{{action('\Modules\Supplier\Http\Controllers\SupplierController@index')}}">@lang('supplier::lang.title')</a>
        </li>
        <li>
            <a href="{{action('\Modules\Supplier\Http\Controllers\ProductController@index')}}">@lang('lang_v1.list_products')</a>
        </li>
        <li class="active">@lang('product.add_product')</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('\Modules\Supplier\Http\Controllers\ProductController@store'), 'method' => 'post',
	'id' => 'product_add_form','class' => 'product_form', 'files' => true ]) !!}
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">
				<div class="col-sm-3">
					{!! Form::label('image', __('lang_v1.product_image') . ':') !!}
					{!! Form::file('image', ['id' => 'upload_image', 'accept' => 'image/*']); !!}
					<small><p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)]) <br> @lang('lang_v1.aspect_ratio_should_be_1_1')</p></small>
				</div>
				<div class="col-sm-9">
					<div class="row form-group">
						<div class="col-sm-8">
						{!! Form::label('name', __('product.product_name') . ':*') !!}
						{!! Form::text('name', !empty($duplicate_product->name) ? $duplicate_product->name : null, ['class' => 'form-control', 'required',
						'placeholder' => __('product.product_name')]); !!}
						</div>
						<div class="col-sm-4">
							{!! Form::label('sku', __('product.sku') . ':') !!} @show_tooltip(__('tooltip.sku'))
							{!! Form::text('sku', null, ['class' => 'form-control',
							  'placeholder' => __('product.sku')]); !!}
						</div>
					</div>

					<div class="row form-group">
						<div class="col-sm-4">
							{!! Form::label('brand_id', __('product.brand') . ':') !!}
							<div class="input-group">
								{!! Form::select('brand_id', $brands, !empty($duplicate_product->brand_id) ? $duplicate_product->brand_id : null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
								<span class="input-group-btn">
								<button type="button" @if(!auth()->user()->can('brand.create')) disabled @endif class="btn btn-default bg-white btn-flat btn-modal" data-href="{{action('BrandController@create', ['quick_add' => true])}}" title="@lang('brand.add_brand')" data-container=".view_modal"><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
							</span>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="@if(!session('business.enable_category')) hide @endif">
								{!! Form::label('category_id', __('product.category') . ':') !!}
								{!! Form::select('category_id', $categories, !empty($duplicate_product->category_id) ? $duplicate_product->category_id : null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
							</div>
						</div>
						<div class="col-sm-4 @if(!(session('business.enable_category') && session('business.enable_sub_category'))) hide @endif">
							{!! Form::label('sub_category_id', __('product.sub_category') . ':') !!}
							{!! Form::select('sub_category_id', $sub_categories, !empty($duplicate_product->sub_category_id) ? $duplicate_product->sub_category_id : null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2']); !!}
						</div>
					</div>

					<div class="row form-group">
						<div class="col-md-4">
							{!! Form::label('unit_id', __('product.unit') . ':*') !!}
							<div class="input-group">
								{!! Form::select('unit_id', $units, !empty($duplicate_product->unit_id) ? $duplicate_product->unit_id : session('business.default_unit'), ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'required']); !!}
								<span class="input-group-btn">
									<button type="button" @if(!auth()->user()->can('unit.create')) disabled @endif class="btn btn-default bg-white btn-flat btn-modal" data-href="{{action('UnitController@create', ['quick_add' => true])}}" title="@lang('unit.add_unit')" data-container=".view_modal"><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
								</span>
							</div>
						</div>
						<div class="col-md-4">
							<br>
							<label>{!! Form::checkbox('enable_stock', 1, !empty($duplicate_product) ? $duplicate_product->enable_stock : true, ['class' => 'input-icheck', 'id' => 'enable_stock']); !!} <strong>@lang('product.manage_stock')</strong>
							</label>@show_tooltip(__('tooltip.enable_stock'))

						</div>
						<div class="col-md-4">
							<div class="@if(!empty($duplicate_product) && $duplicate_product->enable_stock == 0) hide @endif" id="alert_quantity_div">
								{!! Form::label('alert_quantity',  __('product.alert_quantity') . ':*') !!} @show_tooltip(__('tooltip.alert_quantity'))
								{!! Form::number('alert_quantity', !empty($duplicate_product->alert_quantity) ? @number_format($duplicate_product->alert_quantity) : 1 , ['class' => 'form-control', 'required',
								'placeholder' => __('product.alert_quantity'), 'min' => '0']); !!}
							</div>
						</div>
					</div>

					<div class="row form-group">


					</div>

					<div class="row">
						<div class="col-sm-3">
							{!! Form::label('weight',  __('lang_v1.weight') . ':') !!}
							{!! Form::text('weight', !empty($duplicate_product->weight) ? $duplicate_product->weight : null, ['class' => 'form-control', 'placeholder' => __('lang_v1.weight')]); !!}
							<div class="cleafix"></div>

							{!! Form::label('type', __('product.product_type') . ':*') !!} @show_tooltip(__('tooltip.product_type'))
							{!! Form::select('type', ['single' => 'Single', 'variable' => 'Variable'], !empty($duplicate_product->type) ? $duplicate_product->type : null, ['class' => 'form-control select2',
							'required', 'data-action' => !empty($duplicate_product) ? 'duplicate' : 'add', 'data-product_id' => !empty($duplicate_product) ? $duplicate_product->id : '0']); !!}
							<p class="help-block hide">
								@lang('product.help_selling_price')
							</p>
							<div class="cleafix"></div>

							<div class="@if(!session('business.enable_price_tax')) hide @endif">
								{!! Form::label('tax_type', __('product.selling_price_tax_type') . ':*') !!}
								{!! Form::select('tax_type', ['inclusive' => __('product.inclusive'), 'exclusive' => __('product.exclusive')], !empty($duplicate_product->tax_type) ? $duplicate_product->tax_type : 'exclusive',
								['class' => 'form-control select2', 'required']); !!}
							</div>

							<div class="cleafix"></div>

							<div class="@if(!session('business.enable_price_tax')) hide @endif">
								{!! Form::label('tax', __('product.applicable_tax') . ':') !!}
								{!! Form::select('tax', $taxes, !empty($duplicate_product->tax) ? $duplicate_product->tax : null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2'], $tax_attributes); !!}
							</div>
						</div>
						<div class="col-sm-9">
							<div class="form-group row" id="product_form_part"></div>
							<input type="hidden" id="variation_counter" value="1">
							<input type="hidden" id="default_profit_percent"
							       value="{{ $default_profit_percent }}">
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<input type="hidden" name="submit_type" id="submit_type">
			<div class="text-center">
				<div class="btn-group">
					@if($selling_price_group_count)
						<button type="submit" value="submit_n_add_selling_prices" class="btn btn-warning submit_product_form">@lang('lang_v1.save_n_add_selling_price_group_prices')</button>
					@endif

					<button id="opening_stock_button" @if(!empty($duplicate_product) && $duplicate_product->enable_stock == 0) disabled @endif type="submit" value="submit_n_add_opening_stock" class="btn bg-purple submit_product_form">@lang('lang_v1.save_n_add_opening_stock')</button>

					<button type="submit" value="save_n_add_another" class="btn bg-maroon submit_product_form">@lang('lang_v1.save_n_add_another')</button>

					<button type="submit" value="submit" class="btn btn-primary submit_product_form">@lang('messages.save')</button>
				</div>

			</div>
		</div>
	</div>
	{!! Form::close() !!}
</section>
<!-- /.content -->

@endsection

@section('javascript')
  @php $asset_v = env('APP_VERSION'); @endphp
  <script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
  <script src="{{ mix('js/supplier.js') }}"></script>
@endsection