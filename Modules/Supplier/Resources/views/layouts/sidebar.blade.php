@inject('request', 'Illuminate\Http\Request')

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{action('HomeController@index')}}">
                    <i class="fa fa-dashboard"></i> <span>
					@lang('home.home')</span>
                </a>
            </li>

            <li class="treeview {{  in_array( $request->segment(1), ['supplier']) ? 'active active-sub' : '' }}">
                <a href="{{action('\Modules\Supplier\Http\Controllers\ProductController@index')}}"><i class="fa fa-address-book"></i> <span>@lang('supplier::lang.title')</span>
                </a>
            </li>
            <li class="treeview {{ in_array($request->segment(2), ['variation-templates', 'products', 'labels', 'import-products', 'import-opening-stock', 'selling-price-group', 'brands', 'units', 'categories']) ? 'active active-sub' : '' }}" id="tour_step5">
                <a href="#" id="tour_step5_menu"><i class="fa fa-cubes"></i> <span>@lang('sale.products')</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    @can('product.view')
                        <li class="{{ $request->segment(2) == 'products' && $request->segment(2) == '' ? 'active' : '' }}"><a href="{{action('\Modules\Supplier\Http\Controllers\ProductController@index')}}"><i class="fa fa-list"></i>@lang('lang_v1.list_products')</a></li>
                    @endcan
                    @can('product.create')
                        <li class="{{ $request->segment(2) == 'products' && $request->segment(2) == 'create' ? 'active' : '' }}"><a href="{{action('\Modules\Supplier\Http\Controllers\ProductController@create')}}"><i class="fa fa-plus-circle"></i>@lang('product.add_product')</a></li>
                    @endcan

                    @can('product.create')
                        <li class="{{ $request->segment(2) == 'variation-templates' ? 'active' : '' }}"><a href="{{action('VariationTemplateController@index')}}"><i class="fa fa-circle-o"></i><span>@lang('product.variations')</span></a></li>
                    @endcan
                </ul>
            </li>
        </ul>

        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
