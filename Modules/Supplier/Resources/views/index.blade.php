@extends('layouts.app')
@section('title', __('lang_v1.suppliers'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> @lang('lang_v1.suppliers')
            <small>@lang('supplier::lang.title')</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{action('HomeController@index')}}">
                    <i class="fa fa-dashboard"></i> <span>
					@lang('home.home')</span>
                </a>
            <li class="active">@lang('supplier::lang.title')</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">@lang('supplier::lang.title')</h3>
            </div>
            <div class="box-body">
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection
