//This file contains all functions used products tab

$(document).ready(function(){
    //If tax rate is changed
    $(document).on( 'change', 'select#tax_reata', function(){
        if($('select#type').val() == 'single'){
            var purchase_exc_tax = __read_number($('input#single_dpp'));
            purchase_exc_tax = (purchase_exc_tax == undefined) ? 0 : purchase_exc_tax;

            var tax_rate = $('select#tax').find(':selected').data('rate');
            tax_rate = (tax_rate == undefined) ? 0 : tax_rate;

            var purchase_inc_tax = __add_percent(purchase_exc_tax, tax_rate);
            __write_number($('input#single_dpp_inc_tax'), purchase_inc_tax);

            var selling_price = __read_number($('input#single_dsp'));
            var selling_price_inc_tax = __add_percent(selling_price, tax_rate);
            __write_number($('input#single_dsp_inc_tax'), selling_price_inc_tax);
        }
    });

});
