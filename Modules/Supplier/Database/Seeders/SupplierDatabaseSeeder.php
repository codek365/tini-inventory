<?php

namespace Modules\Supplier\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SupplierDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $suppliers = [
		    ['id' => '1','supplier_business_name' => 'Supplier 1','name' => 'Michael','email' => null,'tax_number' => '4590091535','city' => 'Ho Chi Minh','state' => 'Ho Chi Minh','country' => 'VietNam','address' => 'Hiệp Bình Chánh','mobile' => '(+84) 97-4401-889','landline' => null,'alternate_number' => null,'pay_term_number' => '15','pay_term_type' => 'days','created_by' => '1','is_default' => '0','deleted_at' => null,'created_at' => '2019-01-03 20:59:38','updated_at' => '2019-06-11 22:21:03'],
		    ['id' => '2','supplier_business_name' => 'Supplier 2','name' => 'Phillip','email' => null,'tax_number' => null,'city' => 'Ho Chi Minh','state' => 'Ho Chi Minh','country' => 'VietNam','address' => null,'mobile' => '(+84) 97-4401-889','landline' => null,'alternate_number' => null,'pay_term_number' => '30','pay_term_type' => 'days','created_by' => '4','is_default' => '0','deleted_at' => null,'created_at' => '2019-04-10 10:04:20','updated_at' => '2019-04-10 10:04:20'],
	    ];
	    DB::table('supplier')->insert($suppliers);
	    
	    $products = [
		    ['id' => '1','name' => 'Men\'s Reverse Fleece Crew','supplier_id' => '1','type' => 'single','unit_id' => '1','brand_id' => '1','category_id' => '1','sub_category_id' => '5','tax' => '1','tax_type' => 'exclusive','enable_stock' => '1','alert_quantity' => '5','sku' => 'AS0001','barcode_type' => 'C128','weight' => null,'image' => 'fleece_crew.jpg','created_by' => '1','created_at' => '2018-01-03 21:29:08','updated_at' => '2018-06-11 07:40:59'],
		    ['id' => '2','name' => 'Levis Men\'s Slimmy Fit Jeans','supplier_id' => '1','type' => 'variable','unit_id' => '1','brand_id' => '1','category_id' => '1','sub_category_id' => '4','tax' => '1','tax_type' => 'exclusive','enable_stock' => '1','alert_quantity' => '10','sku' => 'AS0002','barcode_type' => 'C128','weight' => null,'image' => 'levis_jeans.jpg','created_by' => '1','created_at' => '2018-01-03 21:30:35','updated_at' => '2018-06-11 07:39:24'],
		    ['id' => '3','name' => 'Men\'s Cozy Hoodie Sweater','supplier_id' => '1','type' => 'variable','unit_id' => '1','brand_id' => '2','category_id' => '1','sub_category_id' => '5','tax' => '1','tax_type' => 'exclusive','enable_stock' => '1','alert_quantity' => '10','sku' => 'AS0003','barcode_type' => 'C128','weight' => null,'image' => 'cozy_sweater.jpg','created_by' => '1','created_at' => '2018-01-03 22:51:52','updated_at' => '2018-06-11 07:40:35'],
		    ['id' => '4','name' => 'Puma Brown Sneaker','supplier_id' => '1','type' => 'variable','unit_id' => '1','brand_id' => '5','category_id' => '3','sub_category_id' => '8','tax' => '1','tax_type' => 'exclusive','enable_stock' => '1','alert_quantity' => '5','sku' => 'AS0004','barcode_type' => 'C128','weight' => null,'image' => 'puma_brown_sneaker.jpg','created_by' => '1','created_at' => '2018-01-03 22:54:33','updated_at' => '2018-06-11 07:42:27'],
	    ];
	    
	    DB::table('supplier_products')->insert($products);
	    
	    DB::table('users')
		    ->where('id', 1)
		    ->update(array('supplier_id' => 1));
    }
}
