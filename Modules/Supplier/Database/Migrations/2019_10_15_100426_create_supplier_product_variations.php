<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierProductVariations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('supplier_product_variations', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('name');
		    $table->integer('supplier_product_id')->unsigned();
		    $table->boolean('is_dummy')->default(1);
		    $table->timestamps();
		    //Indexing
		    $table->index('name');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_product_variations');
    }
}
