<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('supplier_business_name')->nullable();
	        $table->string('name');
	        $table->string('email');
	        $table->string('tax_number')->nullable();
	        $table->string('city')->nullable();
	        $table->string('state')->nullable();
	        $table->string('country')->nullable();
	        $table->string('address')->nullable();
	        $table->string('mobile');
	        $table->string('landline')->nullable();
	        $table->string('alternate_number')->nullable();
	        $table->integer('pay_term_number')->nullable();
	        $table->enum('pay_term_type', ['days', 'months'])->nullable();
	        $table->integer('created_by')->unsigned();
	        $table->boolean('is_default')->default(0);
	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
