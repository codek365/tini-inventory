<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_products', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name');
	        $table->integer('supplier_id')->unsigned();
	        $table->enum('type', ['single', 'variable']);
	        $table->integer('unit_id')->unsigned();
	        $table->integer('brand_id')->unsigned()->nullable();
	        $table->integer('category_id')->unsigned()->nullable();
	        $table->integer('sub_category_id')->unsigned()->nullable();
	        $table->integer('tax')->unsigned()->nullable();
	        $table->enum('tax_type', ['inclusive', 'exclusive']);
	        $table->boolean('enable_stock')->default(0);
	        $table->integer('alert_quantity');
	        $table->string('sku');
	        $table->string('weight');
	        $table->string('image');
	        $table->enum('barcode_type', ['C39', 'C128', 'EAN-13', 'EAN-8', 'UPC-A', 'UPC-E', 'ITF-14']);
	        $table->integer('created_by')->unsigned();
	        $table->timestamps();
	
	        //Indexing
	        $table->index('name');
	        $table->index('supplier_id');
	        $table->index('unit_id');
	        $table->index('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_products');
    }
}
