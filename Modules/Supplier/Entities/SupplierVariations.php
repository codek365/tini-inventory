<?php

namespace Modules\Supplier\Entities;

use Illuminate\Database\Eloquent\Model;

class SupplierVariations extends Model
{
	protected $guarded = ['id'];
	
	public function product_variation()
	{
		return $this->belongsTo('Modules\Supplier\Entities\SupplierProductVariations', 'supplier_product_variation_id');
	}
	
	public function product()
	{
		return $this->belongsTo('Modules\Supplier\Entities\SupplierProduct', 'product_id');
	}
	
	/**
	 * Get the sell lines associated with the variation.
	 */
	public function sell_lines()
	{
		return $this->hasMany(\App\TransactionSellLine::class);
	}
	
	/**
	 * Get the location wise details of the the variation.
	 */
	public function variation_location_details()
	{
		return $this->hasMany(\App\VariationLocationDetails::class);
	}
	
	/**
	 * Get Selling price group prices.
	 */
	public function group_prices()
	{
		return $this->hasMany(\App\VariationGroupPrice::class, 'variation_id');
	}
}
