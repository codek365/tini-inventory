<?php

namespace Modules\Supplier\Entities;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
	protected $table = 'supplier';
    protected $fillable = [];
	
	public function supplier()
	{
		return $this->hasOne(\App\User::class, 'id', 'supplier_id');
	}
}
