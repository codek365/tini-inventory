<?php

namespace Modules\Supplier\Entities;

use Illuminate\Database\Eloquent\Model;

class SupplierProductVariations extends Model
{
	protected $guarded = ['id'];
	public function variations()
	{
		return $this->hasMany('Modules\Supplier\Entities\SupplierVariations', 'supplier_product_variation_id');
	}
}
