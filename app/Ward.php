<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    /**
     * Get the Business city.
     */
    public function city()
    {
        return $this->belongsTo(\App\City::class);
    }
}
