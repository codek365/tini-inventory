<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'province';
    
    public function city(){
        return $this->belongsTo(\App\City::class);
    }
}
