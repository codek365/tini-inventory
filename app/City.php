<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'province';

    protected $guarded = [];
    public $timestamps = false;
    
    public function country(){
        return $this->belongsTo(Country::class);
    }
   
    public function wards(){
        return $this->hasMany(\App\Ward::class);
    }
}
