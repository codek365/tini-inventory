<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $today = \Carbon::now()->format('Y-m-d H:i:s');
        $packages = [
            ['id' => '1','name' => 'Miễn phí','description' => 'Dùng để kiểm tra chức năng','location_count' => '1','user_count' => '2','product_count' => '10','bookings' => '0','kitchen' => '0','order_screen' => '0','tables' => '0','invoice_count' => '10','interval' => 'months','interval_count' => '1','trial_days' => '10','price' => '0.0000','created_by' => '1','sort_order' => '0','is_active' => '1','deleted_at' => null,'created_at' => $today,'updated_at' => '2018-08-01 20:10:49'],
            ['id' => '2','name' => 'Regular','description' => 'Dùng cho cửa hàng vừa và nhỏ','location_count' => '2','user_count' => '5','product_count' => '50','bookings' => '0','kitchen' => '0','order_screen' => '0','tables' => '0','invoice_count' => '50','interval' => 'months','interval_count' => '1','trial_days' => '10','price' => '30000','created_by' => '1','sort_order' => '1','is_active' => '1','deleted_at' => null,'created_at' => $today,'updated_at' => '2018-08-01 20:12:30'],
            ['id' => '3','name' => 'Unlimited','description' => 'Dùng cho doanh nghiệp vừa và nhỏ','location_count' => '0','user_count' => '0','product_count' => '0','bookings' => '0','kitchen' => '0','order_screen' => '0','tables' => '0','invoice_count' => '0','interval' => 'months','interval_count' => '1','trial_days' => '15','price' => '60000','created_by' => '1','sort_order' => '1','is_active' => '1','deleted_at' => null,'created_at' => $today,'updated_at' => '2018-08-01 20:13:50'],
            ['id' => '4','name' => 'Business','description' => 'Dùng cho hệ thống dữ liệu lớn như siêu thị','location_count' => '10','user_count' => '10','product_count' => '15000','bookings' => '0','kitchen' => '0','order_screen' => '0','tables' => '0','invoice_count' => '1000','interval' => 'months','interval_count' => '1','trial_days' => '10','price' => '259.9900','created_by' => '1','sort_order' => '5','is_active' => '0','deleted_at' => null,'created_at' => $today,'updated_at' => '2018-08-01 20:16:14']
        ];

        DB::table('packages')->insert($packages);
    }
}
