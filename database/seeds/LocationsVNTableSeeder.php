<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LocationsVNTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations_sql = file_get_contents(__DIR__ . '/dumps/locations/vietnam_locations.sql');
        $locations_statements = array_filter(array_map('trim', explode(';', $locations_sql)));
        foreach ($locations_statements as $stmt) {
            DB::statement($stmt);
        }
    }
}
