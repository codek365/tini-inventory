<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->string('description')->nullable();
            $table->integer('location_count')->comment("No. of Business Locations, 0 = infinite option.")->unsigned();
            $table->integer('user_count')->unsigned();
            $table->integer('product_count')->unsigned();
            $table->boolean('bookings')->default('0')->comment("Enable/Disable bookings.");
            $table->boolean('kitchen')->default('0')->comment("Enable/Disable kitchen.");
            $table->boolean('order_screen')->default('0')->comment("Enable/Disable order_screen.");
            $table->boolean('tables')->default('0')->comment("Enable/Disable tables.");
            $table->integer('invoice_count')->unsigned();
            $table->enum('interval', ['days', 'months', 'years'])->default('days');
            $table->integer('interval_count')->unsigned();
            $table->integer('trial_days')->unsigned();
            $table->decimal('price', 20, 0)->nullable();
            $table->integer('sort_order')->unsigned();
            $table->integer('created_by')->unsigned();
            // $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('is_active')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
