/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {
    //Date picker
    $('#transaction_date').datepicker({
        autoclose: true,
        format: datepicker_date_format
    });

    if ($("select#customer_id").length > 0) {
        set_default_customer();
    }

    //Add Product
    if ($('#search_market_product').length > 0) {
        $("#search_market_product").autocomplete({
            source: function source(request, response) {
                var price_group = '';
                if ($('#price_group').length > 0) {
                    price_group = $('#price_group').val();
                }
                $.getJSON("/market/list", { price_group: price_group, location_id: $('input#location_id').val(), term: request.term }, response);
            },
            minLength: 2,
            response: function response(event, ui) {
                if (ui.content.length == 1) {
                    ui.item = ui.content[0];
                    if (ui.item.qty_available > 0) {
                        $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                        $(this).autocomplete('close');
                    }
                } else if (ui.content.length == 0) {
                    swal(LANG.no_products_found).then(function (value) {
                        $('input#search_market_product').select();
                    });
                }
            },
            focus: function focus(event, ui) {
                if (ui.item.qty_available <= 0) {
                    return false;
                }
            },
            select: function select(event, ui) {
                if (ui.item.enable_stock != 1 || ui.item.qty_available > 0) {
                    $(this).val(null);
                    add_to_cart(ui.item.variation_id);
                } else {
                    alert(LANG.out_of_stock);
                }
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            var string = "<div>" + item.name;
            if (item.type == 'variable') {
                string += '-' + item.variation;
            }

            var selling_price = item.selling_price;

            string += ' (' + item.sub_sku + ')' + "<br> Price: " + selling_price + "</div>";
            return $("<li>").append(string).appendTo(ul);
        };
    }

    $(document).on('change', '.mfg_date', function () {
        var this_date = $(this).val();
        var this_moment = moment(this_date, moment_date_format);
        var expiry_period = parseFloat($(this).closest('td').find('.row_product_expiry').val());
        var expiry_period_type = $(this).closest('td').find('.row_product_expiry_type').val();
        if (this_date) {
            if (expiry_period && expiry_period_type) {
                exp_date = this_moment.add(expiry_period, expiry_period_type).format(moment_date_format);
                $(this).closest('td').find('.exp_date').datepicker('update', exp_date);
            } else {
                $(this).closest('td').find('.exp_date').datepicker('update', '');
            }
        } else {
            $(this).closest('td').find('.exp_date').datepicker('update', '');
        }
    });

    //Show product list.
    if ($('#product_list_body').length > 0) {
        get_product_list($('input#search_product').val(), $("select#supplier_id").val(), $("select#product_category").val(), $('select#product_brand').val(), $('select#location_id').val(), null);
    }
    $("input#search_product, select#supplier_id, select#location_id, select#product_category, select#product_brand").on("change", function (e) {
        get_product_list($('input#search_product').val(), $("select#supplier_id").val(), $("select#product_category").val(), $('select#product_brand').val(), $('select#location_id').val(), null);
    });

    //Product list pagination
    $(document).on('click', 'ul.pagination_ajax a', function (e) {
        e.preventDefault();
        var term = $('input#search_product').val();
        var supplier_id = $('select#supplier_id').val();
        var location_id = $('select#location_id').val();
        var category_id = $("select#product_category").val();
        var brand_id = $("select#product_brand").val();

        var url = $(this).attr('href');
        get_product_list(term, supplier_id, category_id, brand_id, location_id, url);
    });

    $(document).on('click', '#submit_quick_product', function (e) {
        e.preventDefault();
        $("form#quick_add_product_form").validate({
            rules: {
                sku: {
                    remote: {
                        url: "/products/check_product_sku",
                        type: "post",
                        data: {
                            sku: function sku() {
                                return $("#sku").val();
                            },
                            product_id: function product_id() {
                                if ($('#product_id').length > 0) {
                                    return $('#product_id').val();
                                } else {
                                    return '';
                                }
                            }
                        }
                    }
                },
                expiry_period: {
                    required: {
                        depends: function depends(element) {
                            return $('#expiry_period_type').val().trim() != '';
                        }
                    }
                }
            },
            messages: {
                sku: {
                    remote: LANG.sku_already_exists
                }
            }
        });
        if ($("form#quick_add_product_form").valid()) {
            var form = $("form#quick_add_product_form");
            var url = $(form).attr('action');
            $.ajax({
                method: "POST",
                url: url,
                dataType: 'json',
                data: $(form).serialize(),
                success: function success(data) {
                    $('.quick_add_product_modal').modal('hide');
                    if (data.success) {
                        toastr.success(data.msg);
                        // get_purchase_entry_row(data.product.id, 0);
                    } else {
                        toastr.error(data.msg);
                    }
                }
            });
        }
    });

    $(document).on('change', '.payment-amount', function () {
        cal_balance_due();
    });
});

function add_to_cart(variation_id) {

    var product_row = $('input#product_row_count').val();
    var supplier_id = $('input#supplier_id').val();
    var customer_id = $('select#customer_id').val();
    var is_direct_sell = false;
    if ($('input[name="is_direct_sale"]').length > 0 && $('input[name="is_direct_sale"]').val() == 1) {
        is_direct_sell = true;
    }
    $.ajax({
        method: "GET",
        url: "/market/get_item_cart/" + variation_id + '/' + customer_id,
        async: false,
        data: {
            product_row: product_row,
            customer_id: customer_id,
            is_direct_sell: is_direct_sell
        },
        dataType: "json",
        success: function success(result) {
            if (result.success) {
                $('table#pos_table tbody').append(result.html_content).find('input.pos_quantity');
                //increment row count
                $('input#product_row_count').val(parseInt(product_row) + 1);
                var this_row = $('table#pos_table tbody').find("tr").last();
                pos_each_row(this_row);
                pos_total_row();
                if (result.enable_sr_no == '1') {
                    var new_row = $('table#pos_table tbody').find("tr").last();
                    new_row.find('.add-pos-row-description').trigger('click');
                }

                round_row_to_iraqi_dinnar(this_row);
                __currency_convert_recursively(this_row);
                $('input#search_product').focus().select();

                //Used in restaurant module
                if (result.html_modifier) {
                    $('table#pos_table tbody').find("tr").last().find("td:first").append(result.html_modifier);
                }
            } else {
                swal(result.msg).then(function (value) {
                    $('input#search_product').focus().select();
                });
            }
        }
    });
}
function get_product_list(term, supplier_id, category_id, brand_id, location_id) {
    var url = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

    if (url == null) {
        url = "/market/get_products";
    }
    var form_data = {
        term: term,
        supplier_id: supplier_id,
        category_id: category_id,
        brand_id: brand_id,
        location_id: location_id
    };
    $.ajax({
        method: "GET",
        url: url,
        data: form_data,
        dataType: "html",
        success: function success(result) {
            $('div#product_list_body').html($(result).hide().fadeIn(200));
        }
    });
}

function set_default_customer() {
    var default_customer_id = $('#default_customer_id').val();
    var default_customer_name = $('#default_customer_name').val();
    var exists = $('select#customer_id option[value=' + default_customer_id + ']').length;
    if (exists == 0) {
        $("select#customer_id").append($('<option>', { value: default_customer_id, text: default_customer_name }));
    }

    $('select#customer_id').val(default_customer_id).trigger("change");
}

function cal_balance_due() {
    var total_payable = __read_number($('#final_total_input'));
    var total_paying = 0;
    $('#payment_rows_div').find('.payment-amount').each(function () {
        if (parseFloat($(this).val())) {
            total_paying += __read_number($(this));
        }
    });

    var bal_due = total_payable - total_paying;
    var change_return = 0;

    //change_return
    if (bal_due < 0 || Math.abs(bal_due) < 0.05) {
        __write_number($('input#change_return'), bal_due * -1);
        $('span.change_return_span').text(__currency_trans_from_en(bal_due * -1, true));
        change_return = bal_due * -1;
        bal_due = 0;
    } else {
        __write_number($('input#change_return'), 0);
        $('span.change_return_span').text(__currency_trans_from_en(0, true));
        change_return = 0;
    }

    __write_number($('input#total_paying_input'), total_paying);
    $('span.total_paying').text(__currency_trans_from_en(total_paying, true));

    __write_number($('input#in_balance_due'), bal_due);
    $('span.balance_due').text(__currency_trans_from_en(bal_due, true));

    __highlight(bal_due * -1, $('span.balance_due'));
    __highlight(change_return * -1, $('span.change_return_span'));
}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);