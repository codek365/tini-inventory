$(document).ready( function () {

	$('[data-toggle="tooltip"]').tooltip();
    
	// registration form steps start
	if($("#business_register_form").length){
		var form = $("#business_register_form").show();
	    form.steps({
	        headerTag: "h3",
	        bodyTag: "fieldset",
	        transitionEffect: "slideLeft",
	        labels: {
	        	finish: LANG.register,
	        	next: LANG.next,
	        	previous: LANG.previous
	        },
	        onStepChanging: function (event, currentIndex, newIndex)
	        {
	            // Allways allow previous action even if the current form is not valid!
	            if (currentIndex > newIndex)
	            {
	                return true;
	            }
	            // Needed in some cases if the user went back (clean up)
	            if (currentIndex < newIndex)
	            {
	                // To remove error styles
	                form.find(".body:eq(" + newIndex + ") label.error").remove();
	                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
	            }
	            form.validate().settings.ignore = ":disabled,:hidden";
	            return form.valid();
	        },
	        onFinishing: function (event, currentIndex)
	        {
	            form.validate().settings.ignore = ":disabled";
	            return form.valid();
	        },
	        onFinished: function (event, currentIndex)
	        {
	            form.submit();
	        }
	    });
	}
	// registration form steps end

	//Date picker
    $('.start-date-picker').datepicker({
    	autoclose: true,
    	endDate: 'today'
    });

    $("form#business_register_form").validate({
    	errorPlacement: function(error, element) {
           	if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
         },
    	rules: {
    		name: "required",
    		email: {
		     	email: true
		    },
		    password: {
		    	required: true,
		    	minlength: 5
		    },
		    confirm_password: {
      			equalTo: "#password"
    		},
    		username: {
    			required: true,
		    	minlength: 4,
				remote: {
					url: "/business/register/check-username",
				 	type: "post",
				 	data: {
				 		username: function() {
				 			return $( "#username" ).val();
				 		}
				 	}
				}
    		}
		},
		messages: {
			name: LANG.specify_business_name,
			password: {
		    	minlength: LANG.password_min_length,
			},
			confirm_password: {
				equalTo: LANG.password_mismatch
			},
			username: {
			 	remote: LANG.invalid_username
			}
		}
    });

    $("#business_logo").fileinput({'showUpload':false, 'showPreview':false, 'browseLabel': LANG.file_browse_label, 'removeLabel': LANG.remove});
	
	particlesJS('particles-js',{"particles":{"number":{"value":100,"density":{"enable":true,"value_area":800}},"color":{"value":"#fff"},"shape":{"type":"circle",},"opacity":{"value":.3,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":8,"random":true,"anim":{"enable":false,"speed":1,"size_min":0.5,"sync":false}},"line_linked":{"enable":true,"distance":150,"color":"#fff","opacity":.1,"width":1},"move":{"enable":true,"speed":1,"direction":"none","random":false,"straight":false,"out_mode":"bounce","attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400},"nasa":{"distance":400,"size":50,"duration":2,"opacity":8,"speed":1},"repulse":{"distance":200},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true,});

	// particlesJS.load('particles-js', 'plugins/particles-js/particles.json', function() {
	// 	console.log('callback - particles.js config loaded');
	// });
});