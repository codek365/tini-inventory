---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://posvn.local/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_86a74ead16f16378741b31960309f15f -->
## Returns the authenticated user

> Example request:

```bash
curl -X GET -G "http://posvn.local/api/authenticated_user" 
```

```javascript
const url = new URL("http://posvn.local/api/authenticated_user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/authenticated_user`


<!-- END_86a74ead16f16378741b31960309f15f -->

<!-- START_d5417ec5d425f04b71e9a4e9987c8295 -->
## API Login, on success return JWT Auth token

> Example request:

```bash
curl -X POST "http://posvn.local/api/authenticate" 
```

```javascript
const url = new URL("http://posvn.local/api/authenticate");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST /api/authenticate`


<!-- END_d5417ec5d425f04b71e9a4e9987c8295 -->

<!-- START_39798dab89951f0e0c3fc59a53f859e5 -->
## Log out
Invalidate the token, so user cannot use it anymore
They have to relogin to get a new token

> Example request:

```bash
curl -X POST "http://posvn.local/api/logout" 
```

```javascript
const url = new URL("http://posvn.local/api/logout");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST /api/logout`


<!-- END_39798dab89951f0e0c3fc59a53f859e5 -->

<!-- START_65248d073c7d20410a6e8642cb30abc5 -->
## Refresh the token

> Example request:

```bash
curl -X GET -G "http://posvn.local/api/token" 
```

```javascript
const url = new URL("http://posvn.local/api/token");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response:

```json
null
```

### HTTP Request
`GET /api/token`


<!-- END_65248d073c7d20410a6e8642cb30abc5 -->


