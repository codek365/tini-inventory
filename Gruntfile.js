// Basic Grunt configuration
module.exports = function(grunt) {
    'use strict';
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'public/css/app.css': 'resources/assets/sass/app.scss'
                }
            }
        },
        watch: {
            sass: {
                files: ['resources/assets/sass/*.scss'],
                tasks: ['sass'],
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    'app/*.php', 'public/css/*.css', 'public/js/*.js'
                ]
            },

        },

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['watch']);

};