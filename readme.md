
## Require

    - PHP >= 7.1 (Recommended: PHP version 7.2)
      - OpenSSL PHP Extension
      - PDO PHP Extension
      - Mbstring PHP Extension
      - Tokenizer PHP Extension
      - XML PHP Extension
      - cURL PHP Extension
      - Zip PHP Extension
      - GD PHP Extension
    - MySQL

## Install
- Run `mkdir storage/framework storage/framework/sessions storage/framework/views storage/framework/cache`
- Run `php artisan storage:link`
- Run `chmod -R 775 storage bootstrap/cache public/uploads`
- Run `composer install`
- Run `npm install`
- Run `cp .env.example .env`
- Change database config in `.env`
- Run `php artisan key:generate`
- Run `php artisan migrate --force` To run all of your outstanding migrations
- Run `php artisan db:seed`
- Run `composer dump-autoload` if have new seed
- Only seed demo `php artisan db:seed --class=DummyDemoSeeder` account admin / admin123

## Installation & Supplier module 
###Upload module Supplier to folder Modules and 
- Run `php artisan module:migrate Supplier` 
- Run `php artisan module:seed Supplier`
###Compiling Assets (Laravel Mix)
- Run `cd Modules/Supplier`
- Run `npm install`
- Run `npm run dev` run all Mix tasks
- Run `npm run production` run all Mix tasks and minify output

Goto Admin -> Settings -> Bussiness settings -> On Modules tab enable supplier module -> Save

## Market module 
###Upload module Market to folder Modules and 

- Run `php artisan module:migrate Market` 
###Compiling Assets (Laravel Mix)
- Run `cd Modules/Market`
- Run `npm install`
- Run `npm run dev` run all Mix tasks
- Run `npm run production` run all Mix tasks and minify output

POSVN © 2019 [Huu Ha](mailto:huuhdev@gmail.com) All rights reserved.

## Installation & Documentation
You will find installation guide and documentation in the downloaded zip file.
Also, For complete updated documentation of the ultimate pos please visit online [documentation guide](http://posvn.net/posvn/).

