<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app(Router::class);
// Require Authentication On All Routes
$api->version('v1', ['middleware' => 'api.auth'], function ($api) {
    // Get authenticated user
    $api->get('authenticated_user', 'App\Api\V1\AuthenticateController@authenticatedUser');
    
    // Get a user
    $api->get('users/{user_id}', ['as' => 'users.get', 'uses' => 'App\Api\V1\UserController@get']);
    
    // Update a user
	$api->put('users/{user_id}', ['as' => 'users.update', 'uses' => 'App\Api\V1\UserController@update']);
});


// Require Authentication On Specific Routes
$api->version('v1', function ($api) {
    // API Login, on success return JWT Auth token
    $api->post('authenticate', 'App\Api\V1\AuthenticateController@authenticate');
    
    // Invalidate the token, so user cannot use it anymore
    $api->post('logout', 'App\Api\V1\AuthenticateController@logout');

    // Refresh the token
	$api->get('token', 'App\Api\V1\AuthenticateController@getToken');
    
});